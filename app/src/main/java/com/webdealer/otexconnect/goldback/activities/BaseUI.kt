package com.webdealer.otexconnect.goldback.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.request.RequestOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.*
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.fragments.BaseFragment
import com.webdealer.otexconnect.goldback.helper.PrefManager
import com.webdealer.otexconnect.goldback.models.GlideApp
import uk.co.chrisjenx.calligraphy.CalligraphyConfig
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper
import java.io.IOException


abstract class BaseUI : AppCompatActivity() {
    lateinit var progressBar: Dialog
    lateinit var prefManager: PrefManager
    protected lateinit var paymentID: String
    lateinit var avatar: String
    fun getPrefs(): SharedPreferences {

        return prefManager.pref
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder().setFontAttrId(R.attr.fontPath).build())
        progressBar = BaseFragment.createProgressBar(this)
        prefManager = PrefManager(this)
        paymentID = getPrefs().getString("paymentID", "0")
        avatar = getPrefs().getString("img", "N/A")
    }

//    fun pickImage(baseUI: BaseUI, iPickResult: IPickResult) {
//        PickImageDialog.build(PickSetup().setSystemDialog(true), iPickResult).show(baseUI).apply {
//            setOnPickCancel { dismiss() }
//        }
//    }

    fun getMapMarker(latLng: LatLng): MarkerOptions {
        val options = MarkerOptions()
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location_marker))
        options.position(latLng)

        return options
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase))
    }


    protected fun encodeAddress(address: String): LatLng {
        val coder = Geocoder(this)
        var lat = 0.0
        var lng = 0.0
        try {
            val addresses = coder.getFromLocationName(address, 1) as List<Address>
//            for (add in addresses) {
            if (addresses.isNotEmpty()) {
                val add = addresses[0]
                if (add.hasLatitude() && add.hasLongitude()) {//Controls to ensure it is right address such as country etc.
                    lng = add.longitude
                    lat = add.latitude
                }
            } else Toast.makeText(this, "Address Not Found", Toast.LENGTH_SHORT).show()


        } catch (e: IOException) {
            e.printStackTrace()
        }
        return LatLng(lat, lng)
    }

    protected fun drawPolyLines(googleMap: GoogleMap, from: LatLng, to: LatLng) {
        // val markerOptions = MarkerOptions()   / code for adding marker to polyline as well
        //     markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.Hue))
        //   markerOptions.position(from)
        //   val markerOptions2 = MarkerOptions()
        //   markerOptions2.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
        //  markerOptions2.position(to)
        //  googleMap.addMarker(markerOptions)
        //  googleMap.addMarker(markerOptions2)
        val line = googleMap.addPolyline(PolylineOptions()
                .add(from, to))
        line.color = Color.BLACK
        line.width = 12f
        line.isGeodesic = true
        val cap = BitmapDescriptorFactory.fromResource(R.drawable.line_cap)
        line.startCap = CustomCap(cap)
        line.endCap = CustomCap(cap)
        line.jointType = JointType.ROUND
        val pattern = listOf(Gap(20f), Dash(20f))
        line.pattern = pattern
//        val builder = LatLngBounds.Builder()
//        builder.include(to)
//        builder.include(from)

        try {
            val bounds = LatLngBounds(from, to)
            googleMap.setLatLngBoundsForCameraTarget(bounds)
            googleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 32))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @SuppressLint("MissingPermission")
    protected fun setMapOptions(googleMap: GoogleMap) {
        googleMap.isMyLocationEnabled = true
        googleMap.uiSettings.setAllGesturesEnabled(true)
    }

    companion object {
        @JvmStatic
        var fcmData: Bundle? = null

        @JvmStatic
        fun hasLocationPermission(baseUI: BaseUI): Boolean {
            var permission = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permission = ActivityCompat.checkSelfPermission(baseUI, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseUI, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                if (!permission) {

                    baseUI.requestPermissions(arrayOf(
                            Manifest
                                    .permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION
                    ), 3)
                }
            }
            return permission
        }

        fun loadWithGlide(path: String, iv: ImageView, isCircle: Boolean) {
            val request = GlideApp.with(iv.context).load(path).placeholder(R.drawable.logo)
            if (isCircle)
                request.apply(RequestOptions.circleCropTransform())
            request.into(iv)
        }

        @JvmStatic
        fun loadWithGlide(bitmap: Bitmap, iv: ImageView) {
            GlideApp.with(iv).load(bitmap).placeholder(R.drawable.logo).into(iv)
        }
    }

}