package com.webdealer.otexconnect.goldback.interfaces

import com.webdealer.otexconnect.goldback.models.Order

interface OnCompletedJobsListener {
    fun onJobsCompleted(orders: List<Order>)
    fun onFailed()
}