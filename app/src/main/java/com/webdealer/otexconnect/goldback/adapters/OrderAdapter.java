package com.webdealer.otexconnect.goldback.adapters;

/**
 * Created by fazal on 31/03/2018.
 */

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.activities.JobDetailsBids;
import com.webdealer.otexconnect.goldback.activities.JobDetailsCompleted;
import com.webdealer.otexconnect.goldback.models.Order;

import java.util.List;

import static android.support.v4.content.ContextCompat.startActivity;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.MyViewHolder> {

    private List<Order> orderList;

    public OrderAdapter(List<Order> orderList) {
        this.orderList = orderList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.jobs_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Order job = orderList.get(position);
        holder.title.setText(job.getTitle());
        holder.description.setText(job.getDescription());


        holder.status.setText(job.getStatus());
        holder.pickup_location.setText(job.getPickup_location());
        holder.dropoff_location.setText(job.getDropoff_location());
        holder.budget.setText(job.getBudget());
        if (job.getStatus().equals("3"))
            holder.jobStatus.setImageResource(R.drawable.completedstatus);
        else if (job.getStatus().equals("2"))
            holder.jobStatus.setImageResource(R.drawable.inprogressstatus);


    }

    @Override
    public int getItemCount() {
        return orderList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView title, description, status, pickup_location, dropoff_location, budget;
        public ImageView jobStatus;

        public MyViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.job_row_title);
            description = view.findViewById(R.id.job_row_desc);
            status = view.findViewById(R.id.job_row_status);
            pickup_location = view.findViewById(R.id.job_location_from);
            dropoff_location = view.findViewById(R.id.job_location_to);
            budget = view.findViewById(R.id.job_row_price);
            jobStatus = view.findViewById(R.id.job_status_img);

            itemView.setOnClickListener(this);

        }

        private Class getJobDetail(Order job) {

            if (job.getStatus().equals("3"))
                return JobDetailsCompleted.class;
            else
                return JobDetailsBids.class;


        }

        @Override
        public void onClick(View view) {
            Order order = orderList.get(getAdapterPosition());

            Intent i = new Intent(view.getContext(), getJobDetail(order));
            i.putExtra("title", order.getTitle());
            i.putExtra("id", String.valueOf(order.getId()));
            i.putExtra("driver_id", order.getDriver_id());
            i.putExtra("fleet_id", order.getFleet_id());
            i.putExtra("description", order.getDescription());
            i.putExtra("pickup_location", order.getPickup_location());
            i.putExtra("dropoff_location", order.getDropoff_location());
            i.putExtra("budget", order.getBudget());
            startActivity(view.getContext(), i, null);

        }

    }
}