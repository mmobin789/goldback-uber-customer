package com.webdealer.otexconnect.goldback.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.adapters.CompletedJobsAdapter
import com.webdealer.otexconnect.goldback.interfaces.OnCompletedJobsListener
import com.webdealer.otexconnect.goldback.models.Order
import com.webdealer.otexconnect.goldback.network.RestClient
import kotlinx.android.synthetic.main.fragment_fav_drivers.*

class CompletedJobs : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_fav_drivers, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        getDoneJobs()

    }

    companion object {
        fun newInstance(): CompletedJobs = CompletedJobs()
    }


    private fun getDoneJobs() {
        progressBar.show()
        RestClient.getDoneJobs(getPrefs().getInt("mId", -1), object : OnCompletedJobsListener {
            override fun onJobsCompleted(orders: List<Order>) {
                if (orders.isEmpty()) {
                    errorTV.text = "No Jobs Completed"
                    errorTV.visibility = View.VISIBLE
                } else
                    rv.adapter = CompletedJobsAdapter(orders)
                progressBar.dismiss()
            }

            override fun onFailed() {
                progressBar.dismiss()
            }
        })

    }


}