package com.webdealer.otexconnect.goldback.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.fxn769.Numpad;
import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.models.Sms;
import com.webdealer.otexconnect.goldback.models.Verify;
import com.webdealer.otexconnect.goldback.network.RestClient;
import com.webdealer.otexconnect.goldback.network.WebService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class VerificationActivity extends BaseUI {


    Button mResendCodeButton;
    TextView mVerfiyText, mResendText, mCodeText, mPhoneNumber;
    Typeface typeface;
    WebService mWebService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.activity_verfication_screen);

        mResendCodeButton = findViewById(R.id.resendButton);

        mVerfiyText = findViewById(R.id.verify_text);

        mPhoneNumber = findViewById(R.id.code);

        mResendText = findViewById(R.id.resende_text);
        mResendText.setTypeface(typeface);
        mCodeText = findViewById(R.id.code);
        mCodeText.setTypeface(typeface);
        Numpad numpad = findViewById(R.id.num);


        sendsms();

        final PinEntryEditText pinEntry = findViewById(R.id.txt_pin_entry);

        if (pinEntry != null) {
            pinEntry.setOnPinEnteredListener(str -> {

                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                String mVerificationCode = prefs.getString("mVerificationCode", "0000");

                hideSoftKeyboard();


                if (str.toString().equals(mVerificationCode)) {

                    verifyUser();

                } else {
                    Toast.makeText(getApplication(), "Invalid Code ", Toast.LENGTH_SHORT).show();
                }


            });
        }
        numpad.setOnTextChangeListner((String text, int digits_remaining) -> {
            pinEntry.setText(text);
        });


        mResendCodeButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                sendsms();
            }
        });
    }

    private void sendsms() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String mPhone = prefs.getString("mPhone", "0000000000");

        try {


            RestClient.getServices().sendSMS(mPhone).enqueue(new Callback<Sms>() {

                @Override
                public void onResponse(Call<Sms> call, Response<Sms> response) {
                    SharedPreferences.Editor prefEditor = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit();
                    prefEditor.putString("mVerificationCode", response.body().getFourDigitCode());
                    prefEditor.apply();
                    mPhoneNumber.setText("To " + mPhone);
                    Toast.makeText(getApplicationContext(), "Code Send", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<Sms> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                }
            });

        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), "Error sending sms " + ex.getMessage(), Toast.LENGTH_LONG).show();
            Log.e("Error sms", ex.getMessage());
        }


    }


    private void verifyUser() {


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int mId = prefs.getInt("mId", 0);
        String mType = prefs.getString("Type", "customer");


        RestClient.getServices().Verify(mId, mType).enqueue(new Callback<Verify>() {

            @Override
            public void onResponse(Call<Verify> call, Response<Verify> response) {


                Toast.makeText(getApplicationContext(), "User Verified " + mId, Toast.LENGTH_SHORT).show();
                Intent myIntent = new Intent(getApplicationContext(), PostJobActivity.class);
                startActivity(myIntent);
            }

            @Override
            public void onFailure(Call<Verify> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Failed", Toast.LENGTH_LONG).show();
            }
        });


    }


    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }


}