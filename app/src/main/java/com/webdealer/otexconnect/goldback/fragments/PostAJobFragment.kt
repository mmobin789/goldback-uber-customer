package com.webdealer.otexconnect.goldback.fragments


import android.app.Activity.RESULT_OK
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.PopupMenu
import android.widget.Toast
import com.google.android.gms.location.places.ui.PlacePicker
import com.stripe.android.TokenCallback
import com.stripe.android.model.Card
import com.stripe.android.model.Token
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.activities.ActivityQR
import com.webdealer.otexconnect.goldback.activities.PaymentActivity
import com.webdealer.otexconnect.goldback.adapters.FavDriversAdapter
import com.webdealer.otexconnect.goldback.interfaces.*
import com.webdealer.otexconnect.goldback.models.*
import com.webdealer.otexconnect.goldback.network.RestClient
import kotlinx.android.synthetic.main.activity_post_a_job.*
import kotlinx.android.synthetic.main.choosefav.*
import kotlinx.android.synthetic.main.payment_dialog.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*

// TODO: Rename parameter arguments, choose names that match


/**
 * A simple [Fragment] subclass.
 * Use the [PostAJobFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PostAJobFragment : BaseFragment(), View.OnClickListener {
    private var clickedPlace = ""
    private var clickedTime = ""
    private var descID = 0
    private var descriptions: List<Description>? = null

//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }
//    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
//        return TextView(activity).apply {
//            setText(R.string.hello_blank_fragment)
//        }
        return inflater.inflate(R.layout.activity_post_a_job, container, false)
    }

    private fun timePickerDialog(hour: Int, minutes: Int, pickup: Boolean) {
        val tpd = TimePickerDialog(context, TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->

            val time = "$hourOfDay : $minute"
            if (pickup)
                Pickup_time.append(" $time")
            else Dropoff_time.append(" $time")
        }, hour, minutes, false)
        tpd.show()
    }

    private fun showDescriptions(v: View) {
        val popupMenu = PopupMenu(v.context, v)
        val menu = popupMenu.menu
        descriptions!!.distinctBy {
            menu.add(0, it.id, 0, it.name)
        }
        popupMenu.show()
        popupMenu.setOnMenuItemClickListener {
            Job_desc.setText(it.title)
            descID = it.itemId
            true
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val myCalendar = Calendar.getInstance()
        val date = { _: View, year: Int, monthOfYear: Int, dayOfMonth: Int ->
            // TODO Auto-generated method stub
            val isPickup = if (clickedTime == "Pickup") {
                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)


                val myFormat = "MM/dd/yy" //In which you need put here
                val sdf = SimpleDateFormat(myFormat, Locale.US)

                Pickup_time.text = sdf.format(myCalendar.time)
                true
            }
            //  if (clickedTime == "Dropoff")
            else {

                myCalendar.set(Calendar.YEAR, year)
                myCalendar.set(Calendar.MONTH, monthOfYear)
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                val myFormat = "MM/dd/yy" //In which you need put here
                val sdf = SimpleDateFormat(myFormat, Locale.US)

                Dropoff_time.text = sdf.format(myCalendar.time)
                false
            }
            timePickerDialog(myCalendar.get(Calendar.HOUR_OF_DAY), myCalendar.get((Calendar.MINUTE)), isPickup)

        }

        Dropoff_time.setOnClickListener {
            clickedTime = "Dropoff"
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DATE, -1)

//Set yesterday time milliseconds as date pickers minimum date
            val datePickerDialog = DatePickerDialog(it.context, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH))
            datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000

            datePickerDialog.show()
        }
        Pickup_time.setOnClickListener {
            clickedTime = "Pickup"

            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DATE, -1)

//Set yesterday time milliseconds as date pickers minimum date
            val datePickerDialog = DatePickerDialog(it.context, date, myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH))
            datePickerDialog.datePicker.minDate = System.currentTimeMillis() - 1000

            datePickerDialog.show()
        }

        Pickup_location.setOnClickListener(this)
        Dropoff_location.setOnClickListener(this)
        submit_btn.setOnClickListener(this)
        Job_desc.setOnClickListener(this)
        //checkPayment(false)

        RestClient.getFormData(object : OnFormDataListener {
            override fun onFormData(average: String, descriptions: List<Description>) {
                this@PostAJobFragment.descriptions = descriptions
                rate.setText(average)
                descID = descriptions[0].id
            }
        })
    }

    private fun checkPayment(postJob: Boolean) {
        progressBar.show()

        RestClient.checkBalance(getPrefs().getInt("mId", -1), object : OnBalanceListener {
            override fun onBalance(balance: Float) {
                progressBar.dismiss()
                val budget = budget.text.toString()
                val amount = if (budget.isBlank())
                    0f
                else budget.toFloat()
                if (balance <= 0f || balance < amount) {
                    val message = "Your minimum balance is $balance and job posted is for $amount"
                    if (paymentID == "0") {


                        val intent = Intent(context, PaymentActivity::class.java)
                        intent.flags = Intent.FLAG_ACTIVITY_SINGLE_TOP
                        startActivity(intent)
                    } else {

                        takePayment(message)

                    }
                } else if (postJob) {
                    chooseFavouriteDriver()

                }
            }

        })

    }

    private fun chooseFavouriteDriver() {
        val dialog = Dialog(context)
        dialog.setCancelable(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.choosefav)
        dialog.toolbar.visibility = View.VISIBLE
        dialog.rv.layoutManager = LinearLayoutManager(context)
        progressBar.show()
        RestClient.listFavouriteDrivers(getPrefs().getInt("mId", -1), object : OnFavouriteDriversListener {
            override fun onFavouriteDriversList(favourites: MutableList<FavouriteDriver>?) {
                if (favourites != null && favourites.isNotEmpty()) {
                    val adapter = FavDriversAdapter(this@PostAJobFragment, favourites, true)
                    adapter.onListItemClickListener = object : OnListItemClickListener {
                        override fun onClick(position: Int) {
                            val driverID = favourites[position].driverID
                            val fleetID = favourites[position].driver[0].fleetId
                            progressBar.show()
                            RestClient.addJobToFavourite(driverID, fleetID, getPrefs().getInt("mId", -1), Job_title.text.toString(), Job_desc.text.toString(), Pickup_location.text.toString(), Pickup_time.text.toString(), Dropoff_location.text.toString(), Dropoff_time.text.toString(), budget.text.toString(), num.text.toString()
                                    , object : OnJobAddedListener {
                                override fun onJobAdded(job: Job) {
                                    progressBar.dismiss()

                                    Toast.makeText(context, "Job Added", Toast.LENGTH_SHORT).show()
                                    val intent = Intent(context, ActivityQR::class.java)
                                    intent.putExtra("boxes", num.text.toString().toInt())
                                    intent.putExtra("Order_ID", job.id)
                                    dialog.dismiss()
                                    startActivity(intent)
                                }
                            })
                        }
                    }
                    dialog.rv.adapter = adapter

                }
                progressBar.dismiss()
            }
        })
        dialog.cancel.setOnClickListener {
            dialog.dismiss()
            addPost()
        }
        dialog.show()
    }

    private fun takePayment(message: String) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.payment_dialog)
        dialog.show()
        dialog.message.text = message
        dialog.submit.setOnClickListener {
            val amount = dialog.etPayment.text.toString()
            when {
                amount.isBlank() -> Toast.makeText(it.context, "Enter Amount", Toast.LENGTH_SHORT).show()
                amount.toFloat() < budget.text.toString().toFloat() -> Toast.makeText(it.context, "Minimum ${budget.text} $ balance Required.", Toast.LENGTH_SHORT).show()
                else -> {
                    progressBar.show()
                    getCustomerInfo(dialog, amount.toFloat())
                }
            }

        }

    }

    private fun validDates(fromDate: String, toDate: String): Boolean {
        val sdf = SimpleDateFormat("MM/dd/yy hh : mm", Locale.getDefault())
        return if (fromDate.isNotBlank() && toDate.isNotBlank()) {
            val date1 = sdf.parse(fromDate)
            val date2 = sdf.parse(toDate)
            if (date1.after(date2))
                errorDialog(context!!, "Dates Error", "From Date can't be before")
            return date1.before(date2)
        } else false
    }

    override fun onClick(view: View) {
        if (view.id == R.id.submit_btn) {
            if (validate()) {
                checkPayment(true)


            } else {
                Toast.makeText(context, "Please enter details", Toast.LENGTH_SHORT).show()
            }

            //
        } else if (view.id == R.id.Pickup_location || view.id == R.id.Dropoff_location) {


            if (view.id == R.id.Pickup_location) {
                clickedPlace = "Pickup"
            }

            if (view.id == R.id.Dropoff_location) {
                clickedPlace = "Dropoff"
            }

            val builder = PlacePicker.IntentBuilder()
            startActivityForResult(builder.build(activity), 1)


        } else {
            if (descriptions != null)
                showDescriptions(view)
            else Toast.makeText(view.context, "Descriptions N/A", Toast.LENGTH_SHORT).show()
        }
    }

    private fun validate(): Boolean {

        return validDates(Pickup_time.text.toString(), Dropoff_time.text.toString()) && Job_title.text.toString() != "" && !Job_desc.text.toString().isEmpty() && Pickup_location.text.toString() != "" && !Dropoff_location.text.toString().isEmpty() && !num.text.toString().isEmpty() && !budget.text.toString().isEmpty()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (clickedPlace == "Pickup") {
            if (resultCode == RESULT_OK) {
                val place = PlacePicker.getPlace(activity, data)
                //  val toastMsg = String.format("Place: %s", place.address)
                Pickup_location.text = place.address
                //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        } else {
            if (resultCode == RESULT_OK) {
                val place = PlacePicker.getPlace(activity, data)
                //  val toastMsg = String.format("Place: %s", place.getAddress())
                Dropoff_location.text = place.address
                //Toast.makeText(this, toastMsg, Toast.LENGTH_LONG).show();
            }
        }

    }

    private fun addPost() {
        progressBar.show()
        val mId = getPrefs().getInt("mId", 0)


        RestClient.getServices().addJob(mId, Job_title.text.toString(), descID.toString(), Pickup_location.text.toString(), Pickup_time.text.toString(), Dropoff_location.text.toString(), Dropoff_time.text.toString(), budget.text.toString(), num.text.toString())
                .enqueue(object : Callback<APIResponse> {
                    override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                        if (response.body()!!.status) {
                            progressBar.dismiss()
                            Toast.makeText(context, "Job Added", Toast.LENGTH_SHORT).show()
                            val intent = Intent(context, ActivityQR::class.java)
                            intent.putExtra("boxes", num.text.toString().toInt())
                            intent.putExtra("Order_ID", response.body()!!.data.job.id)
                            startActivity(intent)


                        }


                    }

                    override fun onFailure(call: Call<APIResponse>, t: Throwable) {
                        Toast.makeText(context, "No Internet Connection, Errmsg" + t.message, Toast.LENGTH_SHORT).show()
                        progressBar.dismiss()
                    }
                })


    }

    private fun getCustomerInfo(dialog: Dialog, amount: Float) {
        RestClient.getCustomerPaymentInfo(paymentID, object : OnPaymentInfoListener {
            override fun onPaymentInfo(payment: Payment) {
                resendStripeToken(dialog, payment, amount)
            }
        })
    }

    private fun resendStripeToken(dialog: Dialog, payment: Payment, amount: Float) {

        val card = Card(payment.ccNumber, payment.expiryMonth.toInt(), payment.expiryYear.toInt(), payment.cvc)
        RestClient.stripe(card, context, object : TokenCallback {
            override fun onError(error: java.lang.Exception) {
                Log.e("Stripe", error.toString())
            }

            override fun onSuccess(token: Token) {
                RestClient.customerPay(token.id, getPrefs().getInt("mId", -1), amount, object : OnPaymentListener {
                    override fun onPay() {
                        progressBar.dismiss()
                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
                        submit_btn.isEnabled = true
                        submit_btn.text = "Submit"
                        dialog.dismiss()
                    }
                })

            }
        })
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PostAJobFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
                PostAJobFragment().apply {
                    arguments = Bundle().apply {
                        //                        putString(ARG_PARAM1, param1)
//                        putString(ARG_PARAM2, param2)
                    }
                }
    }
}
