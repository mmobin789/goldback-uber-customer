package com.webdealer.otexconnect.goldback.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.activities.NotificationSettings
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : BaseFragment() {
    companion object {
        @JvmStatic
        fun newInstance() = SettingsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        notification.setOnClickListener {
            startActivity(Intent(it.context, NotificationSettings::class.java))
        }
    }
}