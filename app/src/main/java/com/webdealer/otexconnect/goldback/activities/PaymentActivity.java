package com.webdealer.otexconnect.goldback.activities;


/**
 * Created by fazal on 18/03/2018.
 */

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.helper.DateValidator;
import com.webdealer.otexconnect.goldback.models.AddPayment;
import com.webdealer.otexconnect.goldback.network.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class PaymentActivity extends BaseUI implements View.OnClickListener {

    EditText mCardHolderName;
    EditText mCardNumber;
    EditText mCardExpiry_mm;
    EditText mCardExpiry_yy;
    EditText mCardCvv;
    Button mSubmitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_payment_method);
        initView();


    }

    private void initView() {

        mCardHolderName = findViewById(R.id.card_holder_name);
        mCardNumber = findViewById(R.id.card_number);
        mCardExpiry_mm = findViewById(R.id.card_expire_mm);
        mCardExpiry_yy = findViewById(R.id.card_expire_yy);
        mCardCvv = findViewById(R.id.card_cvv);
        mSubmitBtn = findViewById(R.id.submit_btn);
        mSubmitBtn.setOnClickListener(this);


    }
//
//    private void getStripeToken() {
//        progressBar.show();
//        String cardNo = mCardNumber.getText().toString();
//        int expiryM = Integer.parseInt(mCardExpiry_mm.getText().toString());
//        int expiryY = Integer.parseInt(mCardExpiry_yy.getText().toString());
//        String cvc = mCardCvv.getText().toString();
//        Card card = new Card(cardNo, expiryM, expiryY, cvc);
//        RestClient.stripe(card, this, new TokenCallback() {
//            @Override
//            public void onError(Exception error) {
//                Log.e("Stripe", error.toString());
//                Toast.makeText(mCardCvv.getContext(), error.toString(), Toast.LENGTH_LONG).show();
//                progressBar.dismiss();
//            }
//
//            @Override
//            public void onSuccess(Token token) {
//                Log.i("Stripe", token.getId());
//                progressBar.dismiss();
//                getPrefs().edit().putString("stripe_token", token.getId()).apply();
//                addPayment();
//
//            }
//        });
//
//    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.submit_btn) {
            if (validate()) {
                addPayment();

            } else {
                Toast.makeText(this, "Please Enter valid card details", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void addPayment() {
        progressBar.show();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int mId = prefs.getInt("mId", 0);


        RestClient.getServices().addPayment(mId, mCardHolderName.getText().toString(), mCardNumber.getText().toString(), mCardCvv.getText().toString(), mCardExpiry_mm.getText().toString(), mCardExpiry_yy.getText().toString())
                .enqueue(new Callback<AddPayment>() {
                    @Override
                    public void onResponse(@NonNull Call<AddPayment> call, @NonNull Response<AddPayment> response) {
                        if (response.body().getStatus()) {

                            Toast.makeText(getApplicationContext(), "Payment Method Added", Toast.LENGTH_SHORT).show();

                            getPrefs().edit().putString("paymentID", response.body().getPayment_id()).apply();
                            finish();

                        } else {

                            Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        progressBar.dismiss();

                    }

                    @Override
                    public void onFailure(@NonNull Call<AddPayment> call, @NonNull Throwable t) {
                        Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                        progressBar.dismiss();

                    }
                });

    }


    private boolean validate() {

        if (mCardHolderName.getText().toString().equals("") || mCardNumber.getText().toString().isEmpty() || mCardCvv.getText().toString().equals("") || mCardExpiry_mm.getText().toString().isEmpty() || mCardExpiry_yy.getText().toString().isEmpty()) {
            return false;
        }

        if (!DateValidator.isValid(mCardExpiry_mm.getText().toString(), mCardExpiry_yy.getText().toString())) {

            Toast.makeText(getApplicationContext(), "Invalid expiry date", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


}
