package com.webdealer.otexconnect.goldback.interfaces

import com.webdealer.otexconnect.goldback.models.FavouriteDriver

interface OnFavouriteDriversListener {
    fun onFavouriteDriversList(favourites: MutableList<FavouriteDriver>?)
}