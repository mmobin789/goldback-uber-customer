package com.webdealer.otexconnect.goldback.helper;

/**
 * Created by fazal on 26/03/2018.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.webdealer.otexconnect.goldback.models.ChatMessage;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class PrefManager {
    // Shared preferences file name
    private static final String PREF_NAME = "GoldBack";
    // All Shared Preferences Keys
    private static final String KEY_IS_WAITING_FOR_SMS = "IsWaitingForSms";
    private static final String KEY_MOBILE_NUMBER = "mobile_number";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final Integer KEY_ID = 0;
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOBILE = "mobile";
    // Shared Preferences
    public SharedPreferences pref;
    // Editor for Shared preferences
    Editor editor;
    // Context
    Context _context;
    private Gson gson = new Gson();

    public PrefManager(Context context) {
        this._context = context;
        pref = PreferenceManager.getDefaultSharedPreferences(context);
        editor = pref.edit();
    }


    public void setIsWaitingForSms(boolean isWaiting) {
        editor.putBoolean(KEY_IS_WAITING_FOR_SMS, isWaiting);
        editor.commit();
    }

    public List<ChatMessage> loadChat(String orderID) {

        Type listType = new TypeToken<List<ChatMessage>>() {
        }.getType();
        String json = pref.getString(orderID, "");
        List<ChatMessage> list = gson.fromJson(json, listType);
        if (list == null)
            list = new ArrayList<>();
        return list;
    }

//    public List<RemoteMessage> loadNotifications() {
//        Type listType = new TypeToken<List<RemoteMessage>>() {
//        }.getType();
//        int uid = pref.getInt("mId", -1);
//        String json = pref.getString(uid + "", "");
//        List<RemoteMessage> list = gson.fromJson(json, listType);
//        if (list == null)
//            list = new ArrayList<>();
//        return list;
//
//    }

//    public void saveNotifications(List<RemoteMessage> notifications) {
//        int uid = pref.getInt("mId", -1);
//        Type listType = new TypeToken<List<RemoteMessage>>() {
//        }.getType();
//        String json = gson.toJson(notifications, listType);
//        editor.putString(uid + "", json).apply();
//
//    }

    public void saveChat(String orderID, List<ChatMessage> chatMessageList) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<ChatMessage>>() {
        }.getType();
        String json = gson.toJson(chatMessageList, listType);
        editor.putString(orderID, json).apply();

    }

    public boolean isWaitingForSms() {
        return pref.getBoolean(KEY_IS_WAITING_FOR_SMS, false);
    }

    public String getMobileNumber() {
        return pref.getString(KEY_MOBILE_NUMBER, null);
    }

    public void setMobileNumber(String mobileNumber) {
        editor.putString(KEY_MOBILE_NUMBER, mobileNumber);
        editor.commit();
    }

    public void createLogin(Integer mId, String name, String email, String mobile) {
        editor.putInt(String.valueOf(KEY_ID), mId);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.commit();
    }


    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();
        profile.put("name", pref.getString(KEY_NAME, null));
        profile.put("email", pref.getString(KEY_EMAIL, null));
        profile.put("mobile", pref.getString(KEY_MOBILE, null));
        return profile;
    }
}
