package com.webdealer.otexconnect.goldback.adapters

import android.app.Dialog
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.activities.BaseUI.Companion.loadWithGlide
import com.webdealer.otexconnect.goldback.activities.ChatActivity
import com.webdealer.otexconnect.goldback.helper.PrefManager
import com.webdealer.otexconnect.goldback.interfaces.OnMessageListener
import com.webdealer.otexconnect.goldback.models.ChatMessage
import com.webdealer.otexconnect.goldback.network.RestClient
import kotlinx.android.synthetic.main.receiver.*
import kotlinx.android.synthetic.main.sender.*

class ChatAdapter(private val chatActivity: ChatActivity) : RecyclerView.Adapter<ViewHolder>() {
    private val uid: Int = chatActivity.userID
    private val pref: PrefManager = chatActivity.prefManager
    private val list: MutableList<ChatMessage>
    private val orderID: String = chatActivity.orderID
    private val progressBar: Dialog = chatActivity.progressBar

    init {
        list = pref.loadChat(orderID)
    }

    private val user = 0
    private val driver = 1
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layout = if (viewType == user) {
            R.layout.sender
        } else {
            R.layout.receiver
        }
        return ViewHolder(LayoutInflater.from(parent.context).inflate(layout, parent, false))
    }

    fun receiveMessage(message: String, senderID: Int) {
        val chatMessage = ChatMessage(senderID, message, "N/A")
        list.add(chatMessage)
        notifyItemInserted(list.size)
        pref.saveChat(orderID, list)
    }

    fun sendMessage(message: String) {
        progressBar.show()
        val chatMessage = ChatMessage(uid, message, chatActivity.avatar)
        RestClient.sendMessage(orderID.toInt(), message, object : OnMessageListener {
            override fun onMessageSent() {
                Log.i("Chat", "MessageSent")
                list.add(chatMessage)
                notifyItemInserted(list.size)
                pref.saveChat(orderID, list)
                progressBar.dismiss()

            }
        })

    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        val chatMessage = list[position]
        return if (chatMessage.uid == uid)
            user
        else driver
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val chatMessage = list[position]
        if (chatMessage.uid == uid) // user
        {
            holder.messageSendTV.text = chatMessage.message
            holder.timeSender.text = chatMessage.timestamp
            loadWithGlide(chatMessage.avatar, holder.user, true)
        } else // other
        {
            holder.messageReceiveTV.text = chatMessage.message
            holder.timeReceiver.text = chatMessage.timestamp
        }


    }
}