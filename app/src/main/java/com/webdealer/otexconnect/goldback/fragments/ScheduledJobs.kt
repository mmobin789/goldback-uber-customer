package com.webdealer.otexconnect.goldback.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.adapters.OrderAdapter
import com.webdealer.otexconnect.goldback.models.APIResponse
import com.webdealer.otexconnect.goldback.network.RestClient
import kotlinx.android.synthetic.main.fragment_fav_drivers.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ScheduledJobs : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_fav_drivers, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        getScheduledJobs()

    }

    companion object {
        fun newInstance(): ScheduledJobs = ScheduledJobs()
    }


    private fun getScheduledJobs() {
        progressBar.show()

        RestClient.getServices().getScheduledJobs(getPrefs().getInt("mId", -1)).enqueue(
                object : Callback<APIResponse> {
                    override fun onResponse(call: Call<APIResponse>, response: Response<APIResponse>) {
                        if (response.isSuccessful && response.body()!!.status) {
                            val mlist = response.body()!!.data.orders



                            if (mlist.isEmpty()) {
                                errorTV.text = "No Jobs Scheduled"
                                errorTV.visibility = View.VISIBLE
                            } else {
                                rv.adapter = OrderAdapter(mlist)
                            }


                        }
                        progressBar.dismiss()
                    }

                    override fun onFailure(call: Call<APIResponse>, t: Throwable) {
                        Log.e("error", t.message)
                        progressBar.dismiss()

                    }
                })
    }
}