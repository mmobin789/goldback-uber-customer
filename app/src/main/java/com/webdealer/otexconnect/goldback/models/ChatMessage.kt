package com.webdealer.otexconnect.goldback.models

import com.webdealer.otexconnect.goldback.utils.Utils

class ChatMessage(val uid: Int, val message: String, val avatar: String) {
    val timestamp = Utils.getCurrentTime()
}