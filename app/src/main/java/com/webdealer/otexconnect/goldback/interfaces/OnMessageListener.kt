package com.webdealer.otexconnect.goldback.interfaces

interface OnMessageListener {
    fun onMessageSent()
}