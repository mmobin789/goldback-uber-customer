package com.webdealer.otexconnect.goldback.models;

import com.webdealer.otexconnect.goldback.network.RestClient;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;

/**
 * Created by fazal on 26/03/2018.
 */

public class ErrorUtils {

    public static APIResponse parseError(Response<?> response) {
        Converter<ResponseBody, APIResponse> converter =
                RestClient.retrofit
                        .responseBodyConverter(APIResponse.class, new Annotation[0]);

        APIResponse error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new APIResponse();
        }

        return error;
    }
}
