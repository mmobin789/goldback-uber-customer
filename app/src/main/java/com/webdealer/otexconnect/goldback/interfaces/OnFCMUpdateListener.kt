package com.webdealer.otexconnect.goldback.interfaces

interface OnFCMUpdateListener {
    fun onTokenUpdated()
}