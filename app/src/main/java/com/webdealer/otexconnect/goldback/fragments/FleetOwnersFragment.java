package com.webdealer.otexconnect.goldback.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.adapters.BidsAdapter;
import com.webdealer.otexconnect.goldback.network.RestClient;

/**
 * A simple {@link Fragment} subclass.
 */
public class FleetOwnersFragment extends BaseFragment {

    RecyclerView recycler_view_bids;
    TextView errorTV;

    public FleetOwnersFragment() {
        // Required empty public constructor
    }

    public static FleetOwnersFragment newInstance(int jobID) {
        FleetOwnersFragment fleetOwnersFragment = new FleetOwnersFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("jobID", jobID);
        fleetOwnersFragment.setArguments(bundle);
        return fleetOwnersFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fav_drivers, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        recycler_view_bids = view.findViewById(R.id.rv);
        errorTV = view.findViewById(R.id.errorTV);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view_bids.setLayoutManager(mLayoutManager);
        progressBar.show();
        getFleetOwnerBids(getArguments().getInt("jobID"));


    }


    private void getFleetOwnerBids(int jobID) {
        RestClient.getFleetOwnerBids(jobID, fleetOwnerBids -> {
            if (fleetOwnerBids != null && fleetOwnerBids.isEmpty()) {
                {
                    errorTV.setText("No Fleet Bidded");
                    errorTV.setVisibility(View.VISIBLE);
                }
            } else
                recycler_view_bids.setAdapter(new BidsAdapter(fleetOwnerBids, jobID));
            progressBar.dismiss();
        });
    }

}
