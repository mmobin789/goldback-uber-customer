package com.webdealer.otexconnect.goldback.models

import com.google.gson.annotations.SerializedName

data class FavouriteDriver(

        @SerializedName("customer_id") val cid: Int,
        @SerializedName("driver_id") val driverID: Int,
        val id: Int,
        val driver: List<Driver>

)