package com.webdealer.otexconnect.goldback.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.vansuita.pickimage.listeners.IPickResult
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.activities.BaseUI.Companion.loadWithGlide
import com.webdealer.otexconnect.goldback.interfaces.OnProfileUpdateListener
import com.webdealer.otexconnect.goldback.models.UpdateUser
import com.webdealer.otexconnect.goldback.models.User_
import com.webdealer.otexconnect.goldback.network.RestClient
import kotlinx.android.synthetic.main.fragment_profile.*
import java.io.File

class MyProfileFragment : BaseFragment(), OnProfileUpdateListener {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_profile, container, false)

    }

    companion object {
        @JvmStatic
        fun newInstance() = MyProfileFragment()
    }

    private fun getData(key: String) =
            getPrefs().getString(key, "")

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        etName.setText(getData("mName"))
        etEmail.setText(getData("mEmail"))
        etPhone.setText(getData("mPhone"))
        val uid = getPrefs().getInt("mId", -1)
        save.setOnClickListener {
            val user = UpdateUser(uid, etName.text.toString(), etPhone.text.toString(), etEmail.text.toString())
            if (user.isValidated())
                RestClient.updateProfile(user, this)
            else Toast.makeText(it.context, "Empty Fields", Toast.LENGTH_SHORT).show()
        }
        val avatar = getPrefs().getString("img", "N/A")
        loadWithGlide(avatar, img, true)
        upload.setOnClickListener {
            pickImage(IPickResult {
                if (it.error != null) {
                    Toast.makeText(context, it.error.message, Toast.LENGTH_SHORT).show()
                } else {
                    loadWithGlide(it.path, img, true)
                    getPrefs().edit().putString("img", it.path).apply()
                    progressBar.show()
                    RestClient.uploadUserImage(progressBar, uid.toString(), File(it.path))
                }
            })
        }
    }

    private fun storePrefs(mId: Int, mName: String, mEmail: String, mPhone: String, payment_id: String, ban_status: String, email_verification_status: String, phone_number_verification_status: String) {
        val prefEditor = getPrefs().edit()
        prefEditor.putInt("mId", mId)
        prefEditor.putString("mName", mName)
        prefEditor.putString("mEmail", mEmail)
        prefEditor.putString("mPhone", mPhone)
        prefEditor.putString("paymentID", payment_id)
        prefEditor.putString("ban_status", ban_status)
        prefEditor.putString("email_verification_status", email_verification_status)
        prefEditor.putString("phone_number_verification_status", phone_number_verification_status)

        //prefEditor.putString("fcm_token", fcm_token);
        prefEditor.apply()

    }

    override fun onProfileUpdated(user: User_) {
        storePrefs(user.id, user.name, user.email, user.phoneNumber, user.payment_id, user.banStatus, user.emailVerificationStatus, user.phoneNumberVerificationStatus)
        Toast.makeText(context, "Updated", Toast.LENGTH_SHORT).show()
    }
}