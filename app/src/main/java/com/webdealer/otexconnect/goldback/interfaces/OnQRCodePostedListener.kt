package com.webdealer.otexconnect.goldback.interfaces

interface OnQRCodePostedListener {
    fun onQRCodeSent()
}