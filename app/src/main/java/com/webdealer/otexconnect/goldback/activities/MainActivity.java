package com.webdealer.otexconnect.goldback.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.helper.PrefManager;


public class MainActivity extends BaseUI implements View.OnClickListener {

    LinearLayout mLoginButton;
    TextView mSignup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        BaseUI.hasLocationPermission(this);
        BaseUI.setFcmData(getIntent().getExtras());
    }

    private void initView() {
        mLoginButton = findViewById(R.id.login);
        mSignup = findViewById(R.id.signUp);
        mSignup.setOnClickListener(this);
        mLoginButton.setOnClickListener(this);


        // Check if user is loggedIn
        PrefManager pref = new PrefManager(getApplicationContext());
        if (pref.isLoggedIn()) {
            Intent i = new Intent(MainActivity.this, PostJobActivity.class);
            i.putExtra("desc", getIntent().getStringExtra("desc"));
            i.putExtra("order_id", getIntent().getStringExtra("order_id"));
            i.putExtra("customer_id", getIntent().getStringExtra("customer_id"));
            i.putExtra("title", getIntent().getStringExtra("title"));
            i.putExtra("price", getIntent().getStringExtra("price"));
            startActivity(i);
            finish();
        }
        //-------------------------------

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.login) {
            Intent intent1 = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(intent1);
            // Toast.makeText(this, "Under development!", Toast.LENGTH_SHORT).show();
        }

        if (view.getId() == R.id.signUp) {
            Intent intent = new Intent(this, SignupActivity.class);
            startActivity(intent);
            // Toast.makeText(this, "Under development!", Toast.LENGTH_SHORT).show();
        }
    }
}
