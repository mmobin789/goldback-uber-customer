package com.webdealer.otexconnect.goldback.models

import com.google.gson.annotations.SerializedName

data class Payment(@field:SerializedName("payment_id") val id: String
                   , @field:SerializedName("cc_name") val ccName: String,
                   @field:SerializedName("cc_number") val ccNumber: String,
                   @field:SerializedName("expiry_month") val expiryMonth: String,
                   @field:SerializedName("expiry_year") val expiryYear: String
                   , @field:SerializedName("cvc_number") val cvc: String

)