package com.webdealer.otexconnect.goldback.activities

import android.content.Intent
import android.os.Bundle
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.utils.Utils
import kotlinx.android.synthetic.main.activity_job_detail_completed.*
import kotlinx.android.synthetic.main.job_detail_completed.*

// from scheduled jobs
class JobDetailsCompleted : BaseUI(), OnMapReadyCallback {
    private var p = ""
    private var d = ""
    private var orderID = ""
    lateinit var from: LatLng
    lateinit var to: LatLng
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_job_detail_completed)
        val mapUI = map as SupportMapFragment
        mapUI.getMapAsync(this)
        p = getData("pickup_location")
        d = getData("dropoff_location")
        orderID = getData("id")
        pickUpTV.text = p
        dropOffTV.text = d
        menu.setOnClickListener {
            val tracker = Intent(it.context, TrackDelivery::class.java)
            tracker.putExtra("from", from)
            tracker.putExtra("to", to)
            tracker.putExtra("price", getData("budget"))
            tracker.putExtra("id", orderID)
            tracker.putExtra("distance", distance.text)
            tracker.putExtra("driver_id", getData("driver_id"))
            tracker.putExtra("fleet_id", getData("fleet_id"))
            startActivity(tracker)

        }
        done.setOnClickListener {
            onBackPressed()
        }


    }


    fun getData(key: String): String {
        var s = intent.getStringExtra(key)
        if (s == null)
            s = ""
        return s
    }

    override fun onMapReady(p0: GoogleMap) {
        setMapOptions(p0)
        from = encodeAddress(p)
        to = encodeAddress(d)
        drawPolyLines(p0, from, to)
        distance.text = Utils.meterToMiles(SphericalUtil.computeDistanceBetween(from, to))


    }


}
