package com.webdealer.otexconnect.goldback.activities

import android.os.Bundle
import com.webdealer.otexconnect.goldback.R
import kotlinx.android.synthetic.main.activity_driver_profile.*

class DriverProfile : BaseUI() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_driver_profile)
        if (savedInstanceState != null) {
            val name = savedInstanceState.getString("name")
            val pic = savedInstanceState.getString("pic")
            val car = savedInstanceState.getString("car")
            val carPic = savedInstanceState.getString("car_img")
            val rides = savedInstanceState.getString("rides")
            val rating = savedInstanceState.getString("rating")
            //   val type = savedInstanceState.getString("type")
            loadWithGlide(carPic, car_pic, false)
            loadWithGlide(pic, imageView14, false)
            textView7.text = name
            textView9.text = car
            textView10.text = rating
            textView11.text = rides
        }

    }
}
