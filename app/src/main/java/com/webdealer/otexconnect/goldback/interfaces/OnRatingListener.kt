package com.webdealer.otexconnect.goldback.interfaces

import com.webdealer.otexconnect.goldback.models.Order

interface OnRatingListener {
    fun onRated(order: Order?)
}