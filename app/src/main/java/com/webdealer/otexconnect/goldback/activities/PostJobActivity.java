package com.webdealer.otexconnect.goldback.activities;


/**
 * Created by fazal on 18/03/2018.
 */

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.fragments.BaseFragment;
import com.webdealer.otexconnect.goldback.fragments.ContactUsFragment;
import com.webdealer.otexconnect.goldback.fragments.FavouriteDrivers;
import com.webdealer.otexconnect.goldback.fragments.MyJobsFragment;
import com.webdealer.otexconnect.goldback.fragments.MyProfileFragment;
import com.webdealer.otexconnect.goldback.fragments.Notifications;
import com.webdealer.otexconnect.goldback.fragments.PostAJobFragment;
import com.webdealer.otexconnect.goldback.fragments.SettingsFragment;
import com.webdealer.otexconnect.goldback.helper.PrefManager;
import com.webdealer.otexconnect.goldback.interfaces.OnFCMUpdateListener;
import com.webdealer.otexconnect.goldback.network.RestClient;


public class PostJobActivity extends BaseUI implements View.OnClickListener {

    LinearLayout post_job;
    private PostAJobFragment postAJobFragment;
    private DrawerLayout mDrawerLayout;
    private NavigationView navigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_job);
        post_job = findViewById(R.id.post_job);

        post_job.setOnClickListener(this);


        // Check if user is not loggedIn
        if (!prefManager.isLoggedIn()) {
            // Create Login Session
            SharedPreferences prefs = getPrefs();
            int mId = prefs.getInt("mId", 0);
            String mName = prefs.getString("mName", "DefaultName");
            String mEmail = prefs.getString("mEmail", "email");
            String mPhone = prefs.getString("mPhone", "phone");
            SharedPreferences.Editor editor = prefs.edit();
            editor.putInt("id", mId);
            editor.putString("name", mName);
            editor.putString("email", mEmail);
            editor.putString("mobile", mPhone);
            editor.apply();
            prefManager.createLogin(mId, mName, mEmail, mPhone);
        }
        //-------------------------------

        mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("");
        actionbar.setHomeAsUpIndicator(R.drawable.drmenu);


        navigation = findViewById(R.id.nav_view);
        View hView = navigation.getHeaderView(0);
        TextView mNav_title = hView.findViewById(R.id.nav_title);
        String mEmail = prefManager.pref.getString("mEmail", "email");
        mNav_title.setText(mEmail);
        TextView header = (TextView) toolbar.getChildAt(0);
        findViewById(R.id.notification).setOnClickListener((v -> {
            header.setText("Notifications");
            setFragment(Notifications.newInstance());
        }));
        if (getIntent().getBooleanExtra("jobs", false)) {
            header.setText("Scheduled Jobs");

            setFragment(MyJobsFragment.newInstance());
        }
        postAJobFragment = PostAJobFragment.newInstance();
        navigation.setNavigationItemSelectedListener(item -> {

            int id = item.getItemId();
            BaseFragment fragment = null;
            switch (id) {
                case R.id.nav_logout:

                    Toast.makeText(getApplicationContext(), "Logging out....", Toast.LENGTH_SHORT).show();
                    RestClient.updateFCM(getPrefs().getInt("mId", -1), "", new OnFCMUpdateListener() {
                        @Override
                        public void onTokenUpdated() {

                            SharedPreferences sharedPreferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.clear().apply();
                            PrefManager pref1 = new PrefManager(getApplicationContext());
                            pref1.clearSession();
                            Intent i = new Intent(PostJobActivity.this, MainActivity.class);
                            startActivity(i);
                            finish();

                        }
                    });

                    break;

                case R.id.nav_my_jobs:
                    header.setText("Scheduled Jobs");
                    fragment = MyJobsFragment.newInstance();

                    break;
                case R.id.nav_support:
                    header.setText("Contact Us");
                    fragment = ContactUsFragment.newInstance();
                    break;
                case R.id.nav_fav:
                    header.setText("Favourite Drivers");
                    fragment = FavouriteDrivers.newInstance();
                    break;
                case R.id.nav_post_job:
                    header.setText("Post a Job");
                    fragment = postAJobFragment;
                    break;
                case R.id.nav_profile:
                    header.setText("My Profile");
                    fragment = MyProfileFragment.newInstance();
                    break;
                case R.id.nav_settings:
                    header.setText("Settings");
                    fragment = SettingsFragment.newInstance();
                    break;
                default:
                    Toast.makeText(PostJobActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
                    break;

            }

            setFragment(fragment);
            mDrawerLayout.closeDrawer(GravityCompat.START);
            return false;
        });

    }

    private String getNotificationData() {
        String value = null;
        if (getFcmData() != null)
            value = getFcmData().getString("order");
        if (value != null) {
            Log.i("FCM Data Order", value);

        }

        return value;
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkRejectedJob();

    }

    private void checkRejectedJob() {
        Intent i = new Intent(this, RejectedJob.class);
        if (getNotificationData() != null) {
            startActivity(i);
        } else if (getIntent().getStringExtra("order_id") != null) {
            i.putExtra("desc", getIntent().getStringExtra("desc"));
            i.putExtra("order_id", getIntent().getStringExtra("order_id"));
            i.putExtra("customer_id", getIntent().getStringExtra("customer_id"));
            i.putExtra("title", getIntent().getStringExtra("title"));
            i.putExtra("price", getIntent().getStringExtra("price"));
            startActivity(i);
            getIntent().removeExtra("order_id");


        }


    }


    private void setFragment(BaseFragment fragment) {
        if (fragment != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

//    private void getPrefs() {
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        int mId = prefs.getInt("mId", 0);
//        String mName = prefs.getString("mName", "DefaultName");
//        String mEmail = prefs.getString("mEmail", "email");
//        String mPhone = prefs.getString("mPhone", "phone");
//        String paymentID = prefs.getString("paymentID", "0");
//        String ban_status = prefs.getString("ban_status", "0");
//        String email_verification_status = prefs.getString("email_verification_status", "0");
//        String phone_number_verification_status = prefs.getString("phone_number_verification_status", "0");
//        String mfck = prefs.getString("fcm_token", "0");
//
//    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        postAJobFragment.onActivityResult(requestCode, resultCode, data);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.post_job) {
            setFragment(postAJobFragment);

        }
    }


}






