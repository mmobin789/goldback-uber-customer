package com.webdealer.otexconnect.goldback.models;

import com.google.gson.annotations.SerializedName;

public class User_ {


    @SerializedName("profile_image")
    public String profilePic;
    private String name;
    private String email;
    private String password;
    private int id;
    @SerializedName("phone_number")
    private String phoneNumber;
    private String payment_id;
    @SerializedName("fcm_token")

    private Object fcmToken;
    @SerializedName("ban_status")

    private String banStatus;
    @SerializedName("email_verification_status")

    private String emailVerificationStatus;
    @SerializedName("phone_number_verification_status")

    private String phoneNumberVerificationStatus;
    @SerializedName("created_at")

    private Object createdAt;
    @SerializedName("updated_at")

    private Object updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPayment_id() {
        return payment_id;
    }

    public void setPayment_id(String payment_id) {
        this.payment_id = payment_id;
    }

    public Object getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(Object fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getBanStatus() {
        return banStatus;
    }

    public void setBanStatus(String banStatus) {
        this.banStatus = banStatus;
    }

    public String getEmailVerificationStatus() {
        return emailVerificationStatus;
    }

    public void setEmailVerificationStatus(String emailVerificationStatus) {
        this.emailVerificationStatus = emailVerificationStatus;
    }

    public String getPhoneNumberVerificationStatus() {
        return phoneNumberVerificationStatus;
    }

    public void setPhoneNumberVerificationStatus(String phoneNumberVerificationStatus) {
        this.phoneNumberVerificationStatus = phoneNumberVerificationStatus;
    }

    public Object getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Object createdAt) {
        this.createdAt = createdAt;
    }

    public Object getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Object updatedAt) {
        this.updatedAt = updatedAt;
    }

}
