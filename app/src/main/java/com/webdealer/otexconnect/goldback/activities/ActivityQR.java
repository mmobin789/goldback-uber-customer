package com.webdealer.otexconnect.goldback.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.print.PrintHelper;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.helper.PrefManager;
import com.webdealer.otexconnect.goldback.network.RestClient;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by fazal on 30/03/2018.
 */

public class ActivityQR extends BaseUI implements View.OnClickListener {

    private DrawerLayout mDrawerLayout;
    private NavigationView navigation;
    private ImageView qrPhoto;
    private ImageView mPrintQR;
    private ImageView mSaveQR;
    private LinearLayout optionsLL;
    private Button add, done;
    private String orderID;
    private List<String> qrCodes = new ArrayList<>();
    private Bitmap qrCode;
    private int boxes;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_qr_code);
        qrPhoto = findViewById(R.id.qrPhoto);
        mSaveQR = findViewById(R.id.saveqr);
        mPrintQR = findViewById(R.id.printqr);
        optionsLL = findViewById(R.id.options);
        add = findViewById(R.id.add);
        done = findViewById(R.id.done);
        mSaveQR.setOnClickListener(this);
        mPrintQR.setOnClickListener(this);
        add.setOnClickListener(this);
        done.setOnClickListener(this);
        orderID = getIntent().getStringExtra("Order_ID");
        boxes = getIntent().getIntExtra("boxes", -1);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setTitle("");

        actionbar.setHomeAsUpIndicator(R.drawable.drmenu);

        navigation = findViewById(R.id.nav_view);

        View hView = navigation.getHeaderView(0);
        TextView mNav_title = hView.findViewById(R.id.nav_title);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String mEmail = prefs.getString("mEmail", "email");
        mNav_title.setText(mEmail);
        navigation.setNavigationItemSelectedListener(item -> {

            int id = item.getItemId();
            switch (id) {
                case R.id.nav_logout:

                    Toast.makeText(getApplicationContext(), "Logging out....", Toast.LENGTH_SHORT).show();

                    SharedPreferences sharedPreferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear().apply();
                    PrefManager pref = new PrefManager(getApplicationContext());
                    pref.clearSession();
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    finish();

                    break;
                default:
                    Toast.makeText(getApplicationContext().getApplicationContext(), "Under Development", Toast.LENGTH_SHORT).show();
                    break;


            }

            return false;
        });
        generateQR();
        Log.i("OrderID", orderID);
    }

    private void saveQrCodeToFile() {

        try {
            File root = new File(Environment.getExternalStorageDirectory(), getString(R.string.app_name) + " QR CODES");
            if (!root.exists())
                root.mkdir();
            String name = qrCodes.get(qrCodes.size() - 1);
            File file = new File(root.getAbsolutePath(), name + ".png");
            if (!file.exists())
                file.createNewFile();
            FileOutputStream out = new FileOutputStream(file);
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            qrCode.compress(Bitmap.CompressFormat.PNG, 100, bos); // bmp is your Bitmap instance
            // PNG is a lossless format, the compression factor (100) is ignored
            byte[] bitmapData = bos.toByteArray();
            out.write(bitmapData);
            out.flush();
            out.close();

            Log.i("QRFile", file.getAbsolutePath());

            Toast.makeText(this, "QR Code Saved " + file.getName(), Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void postQRCode() {
        RestClient.createQRCodes(qrCodes, orderID, () -> {
            Toast.makeText(this, "QR Code Posted.", Toast.LENGTH_SHORT).show();
            if (boxesEqualQRCodes()) {
                add.setVisibility(View.GONE);
                Toast.makeText(this, "Number of Boxes: " + qrCodes.size() + "/" + boxes + " Order Post Completed.", Toast.LENGTH_LONG).show();
                loadJobs();
            }

        });
    }


    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.add) {
            generateQR();
        } else if (v.getId() == R.id.saveqr && qrCode != null) {
            saveQrCodeToFile();

        } else if (v.getId() == R.id.printqr) {

            PrintHelper photoPrinter = new PrintHelper(this);
            photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);

            photoPrinter.printBitmap("QRCode Printing", qrCode);
        } else {
            if (boxesEqualQRCodes()) {
                postQRCode();

            } else {
                Toast.makeText(v.getContext(), "Number of Boxes: " + qrCodes.size() + "/" + boxes, Toast.LENGTH_SHORT).show();
            }
        }

    }

    private boolean boxesEqualQRCodes() {
        return boxes == qrCodes.size();
    }

    private void loadJobs() {
        finish();
        Intent intent = new Intent(getBaseContext(), PostJobActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT);
        intent.putExtra("jobs", true);
        startActivity(intent);
    }

    private void generateQR() {

        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {

            String qrCodeS = orderID + System.currentTimeMillis();
            qrCodes.add(qrCodeS);

            BitMatrix bitMatrix = multiFormatWriter.encode(qrCodeS, BarcodeFormat.QR_CODE, 200, 200);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            qrCode = barcodeEncoder.createBitmap(bitMatrix);
            loadWithGlide(qrCode, qrPhoto);
        } catch (WriterException e) {
            e.printStackTrace();
        } finally {
            if (boxesEqualQRCodes())
                add.setVisibility(View.GONE);
        }


    }


}
