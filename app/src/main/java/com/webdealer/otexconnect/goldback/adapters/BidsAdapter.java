package com.webdealer.otexconnect.goldback.adapters;

/**
 * Created by fazal on 31/03/2018.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.activities.DriverProfile;
import com.webdealer.otexconnect.goldback.models.Driver;
import com.webdealer.otexconnect.goldback.models.DriverJob;
import com.webdealer.otexconnect.goldback.network.RestClient;

import java.util.List;

public class BidsAdapter extends RecyclerView.Adapter<BidsAdapter.MyViewHolder> {


    private List<Driver> bidsList;
    private int jobID;

    public BidsAdapter(List<Driver> bidsList, int jobID) {
        this.bidsList = bidsList;
        this.jobID = jobID;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fleet_bids_list_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (bidsList != null) {
            String strDriverName = "", strVehicleName = "", strDriverPhone = "", strVehicleType = "", strBidAmount = "";
            Driver bid = bidsList.get(position);

            if (bid.getName() != null) {
                strDriverName = bid.getName();
            }
            if (bid.getVehicleName() != null) {
                strVehicleName = bid.getVehicleName();
            }
            if (bid.getPhoneNumber() != null) {
                strDriverPhone = bid.getPhoneNumber();
            }
            if (bid.getType() != null) {
                strVehicleType = bid.getType();
            }
            if (bid.getBids() != null && bid.getBids().get(0).getBid() != null) {
                strBidAmount = bid.getBids().get(0).getBid();
            }


            holder.driverName.setText(strDriverName);
            holder.tvVehicleName.setText(strVehicleName);
            holder.tvDriverPhone.setText(strDriverPhone);
            holder.tvVehicleType.setText(strVehicleType);
            holder.tvBidAmount.setText(strBidAmount);
        }

    }

    @Override
    public int getItemCount() {
        if (bidsList != null)
            return bidsList.size();
        else return 10;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView driverName, tvVehicleName, tvDriverPhone, tvVehicleType, tvBidAmount;


        MyViewHolder(View view) {
            super(view);
            driverName = view.findViewById(R.id.tvdriverName);
            tvVehicleName = view.findViewById(R.id.tvVehicleName);
            tvDriverPhone = view.findViewById(R.id.tvdriverPhone);
            tvVehicleType = view.findViewById(R.id.tvVehicleType);
            tvBidAmount = view.findViewById(R.id.tvBidAmount);
            TextView awardJob = view.findViewById(R.id.awardJob);
            TextView profile = view.findViewById(R.id.profileTV);
            profile.setOnClickListener(v -> {
                if (bidsList != null) {
                    Driver driver = bidsList.get(getAdapterPosition());
                    Bundle bundle = new Bundle();
                    bundle.putString("name", driver.getName());
                    bundle.putString("car", driver.getVehicleName());
                    bundle.putString("car_img", driver.getVehicleURL());
                    bundle.putString("rating", driver.getRating());
                    bundle.putString("type", driver.getType());
                    bundle.putString("rides", driver.getRides() + "");
                    bundle.putString("pic", driver.getProfileImage());
                    ContextCompat.startActivity(v.getContext(), new Intent(v.getContext(), DriverProfile.class), bundle);
                } else Toast.makeText(v.getContext(), "Profile N/A", Toast.LENGTH_SHORT).show();
            });
            awardJob.setOnClickListener(v -> {
                int position = getAdapterPosition();
                Driver driver = bidsList.get(position);
                if (driver != null) {
                    RestClient.awardJob(new DriverJob(driver.getId(), jobID), () -> {
                        Toast.makeText(v.getContext(), "Job Awarded", Toast.LENGTH_SHORT).show();
                        bidsList.remove(position);
                        notifyItemRemoved(position);
                    });
                }
            });


        }


    }
}