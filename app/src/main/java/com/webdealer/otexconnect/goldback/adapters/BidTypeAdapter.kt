package com.webdealer.otexconnect.goldback.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.webdealer.otexconnect.goldback.fragments.BaseFragment
import com.webdealer.otexconnect.goldback.fragments.FleetOwnersFragment
import com.webdealer.otexconnect.goldback.fragments.FleetsFragment

class BidTypeAdapter(fm: FragmentManager, private val jobID: Int) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): BaseFragment {
        return when (position) {
            0 -> {
                FleetsFragment.newInstance(jobID)
            }
            else -> {
                FleetOwnersFragment.newInstance(jobID)
            }
        }

    }

    override fun getCount(): Int {
        return 2
    }

}