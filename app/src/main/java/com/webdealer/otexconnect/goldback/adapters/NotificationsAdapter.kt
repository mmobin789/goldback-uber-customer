package com.webdealer.otexconnect.goldback.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.google.firebase.messaging.RemoteMessage
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.models.GlideApp
import kotlinx.android.synthetic.main.notification_row.*

class NotificationsAdapter : RecyclerView.Adapter<ViewHolder>() {
    private val notifications: List<RemoteMessage> = emptyList()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.notification_row, parent, false))
    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val remoteMessage = notifications[position]
        holder.title.text = remoteMessage.notification!!.title
        holder.desc.text = remoteMessage.notification!!.body
        val minutes = (remoteMessage.sentTime / (1000 * 60) % 60)
        holder.time.text = minutes.toString() + " minutes ago"
        GlideApp.with(holder.containerView).load(R.drawable.notificationicon).into(holder.icon)

    }


}