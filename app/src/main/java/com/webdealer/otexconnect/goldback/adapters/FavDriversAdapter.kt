package com.webdealer.otexconnect.goldback.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.activities.BaseUI
import com.webdealer.otexconnect.goldback.fragments.BaseFragment
import com.webdealer.otexconnect.goldback.interfaces.OnJobAwardedListener
import com.webdealer.otexconnect.goldback.interfaces.OnListItemClickListener
import com.webdealer.otexconnect.goldback.models.FavouriteDriver
import com.webdealer.otexconnect.goldback.network.RestClient
import kotlinx.android.synthetic.main.driver_row.*

class FavDriversAdapter(private val baseFragment: BaseFragment, private val list: MutableList<FavouriteDriver>, private val isCallToDriver: Boolean) : RecyclerView.Adapter<ViewHolder>() {

    lateinit var onListItemClickListener: OnListItemClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.driver_row, parent, false))
        if (isCallToDriver) {
            v.delete.visibility = View.GONE
            v.containerView.setOnClickListener {
                onListItemClickListener.onClick(v.adapterPosition)
            }
        }
        v.delete.setOnClickListener {
            baseFragment.progressBar.show()
            RestClient.deleteFavouriteDriver(list[v.adapterPosition].id, object : OnJobAwardedListener {
                override fun onJobAwarded() {
                    list.removeAt(v.adapterPosition)
                    notifyItemRemoved(v.adapterPosition)
                    baseFragment.progressBar.dismiss()
                }
            })
        }
        return v
    }

    override fun getItemCount(): Int {
        return if (list.size <= 5)
            list.size
        else 5
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val driver = list[position].driver[0]
        BaseUI.loadWithGlide(driver.profileImage, holder.imgIV, true)
        holder.name.text = driver.name
        if (driver.phoneNumber.isNullOrBlank())
            holder.phone.visibility = View.INVISIBLE
        else
            holder.phone.text = driver.phoneNumber
    }
}