package com.webdealer.otexconnect.goldback.models

import com.google.gson.annotations.SerializedName

data class UpdateUser(@field:SerializedName("customer_id") val id: Int, val name: String, @field:SerializedName("phone_number") val phone: String, val email: String) {

    fun isValidated() = id != -1 && name.isNotBlank() && email.isNotBlank() && phone.isNotBlank()
}