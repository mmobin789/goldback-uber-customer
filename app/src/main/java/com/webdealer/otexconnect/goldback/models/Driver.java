package com.webdealer.otexconnect.goldback.models;

/**
 * Created by fazal on 10/04/2018.
 */

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Driver {
    @SerializedName("fleet_id")
    public String fleetId = "";
    private String rating;
    private int rides;
    @SerializedName("profile_image")
    private String profileImage = "";
    private String licenseURL = "";
    private String name = "";
    private String email = "";
    private String password = "";
    @SerializedName("phone_number")
    private String phoneNumber = "";
    @SerializedName("vehicleURL")
    private String vehicleURL = "";
    @SerializedName("fcm_token")
    private String fcmToken = "";
    @SerializedName("vehicle_name")
    private String vehicleName = "";
    private int id;
    @SerializedName("type")
    private String type = "";
    @SerializedName("license_verification_status")
    private Integer licenseVerificationStatus = 0;
    @SerializedName("vehicle_verification_status")

    private Integer vehicleVerificationStatus = 0;
    @SerializedName("ban_status")

    private String banStatus = "";
    @SerializedName("email_verification_status")

    private String emailVerificationStatus = "";
    @SerializedName("phone_number_verification_status")

    private String phoneNumberVerificationStatus = "";
    @SerializedName("created_at")

    private String createdAt = "";
    @SerializedName("updated_at")

    private String updatedAt = "";
    private String model = "";
    private String colour = "";
    @SerializedName("register_number")
    private String registerNumber = "";
    private List<Bid> bids;

    public int getRides() {
        return rides;
    }

    public String getRating() {
        return rating;
    }

    public int getId() {
        return id;
    }


    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }


    public String getPassword() {
        return password;
    }


    public String getPhoneNumber() {
        return phoneNumber;
    }


    public String getProfileImage() {
        return profileImage;
    }


    public String getVehicleURL() {
        return vehicleURL;
    }


    public String getType() {
        return type;
    }


    public String getVehicleName() {
        return vehicleName;
    }


    public List<Bid> getBids() {
        return bids;
    }


}

