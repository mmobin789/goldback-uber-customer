package com.webdealer.otexconnect.goldback.interfaces

import com.webdealer.otexconnect.goldback.models.Payment

interface OnPaymentInfoListener {
    fun onPaymentInfo(payment: Payment)
}