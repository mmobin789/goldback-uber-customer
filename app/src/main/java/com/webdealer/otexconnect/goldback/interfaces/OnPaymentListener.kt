package com.webdealer.otexconnect.goldback.interfaces

interface OnPaymentListener {
    fun onPay()
}