package com.webdealer.otexconnect.goldback.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.adapters.BidTypeAdapter;
import com.webdealer.otexconnect.goldback.helper.PrefManager;

public class BidsActivity extends BaseUI {

    TextView schTV, doneTV;
    private DrawerLayout mDrawerLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity__bids);


        //-----------------------------------------------------


        mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setTitle("");
        actionbar.setHomeAsUpIndicator(R.drawable.drmenu);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        NavigationView navigation = findViewById(R.id.nav_view);
        View hView = navigation.getHeaderView(0);
        TextView mNav_title = hView.findViewById(R.id.nav_title);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String mEmail = prefs.getString("mEmail", "email");
        mNav_title.setText(mEmail);
        int jobID = getIntent().getIntExtra("id", -1);
        BidTypeAdapter adapter = new BidTypeAdapter(getSupportFragmentManager(), jobID);

        viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

//        smartTabLay.set(Color.parseColor("#FF0000"));

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {


                if (viewPager.getCurrentItem() == 1) {

                    tab2();
                } else {
                    tab1();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        navigation.setNavigationItemSelectedListener(item -> {

            int id = item.getItemId();
            switch (id) {
                case R.id.nav_logout:

                    Toast.makeText(getApplicationContext(), "Logging out....", Toast.LENGTH_SHORT).show();

                    SharedPreferences sharedPreferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear().apply();
                    PrefManager pref = new PrefManager(getApplicationContext());
                    pref.clearSession();
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    finish();
                    break;
                case R.id.nav_post_job:
                    //Toast.makeText(getApplicationContext(), paymentID, Toast.LENGTH_SHORT).show();

                    if (paymentID.equals("0")) {


                        Intent intent = new Intent(getApplicationContext(), PaymentActivity.class);
                        startActivity(intent);
                    } else {
                        onBackPressed();
                    }


                default:
                    Toast.makeText(getApplicationContext(), "Under Development", Toast.LENGTH_SHORT).show();
                    break;

            }

            DrawerLayout drawer = findViewById(R.id.drawer_layout);
            drawer.closeDrawer(GravityCompat.START);
            return false;
        });


        //--------------------------------------------------------


        schTV = findViewById(R.id.schTV);
        doneTV = findViewById(R.id.doneTV);
        schTV.setOnClickListener(v -> {
            tab1();
            viewPager.setCurrentItem(0);
        });
        doneTV.setOnClickListener(v -> {
                    tab2();
                    viewPager.setCurrentItem(1);
                }
        );

    }

    private void tab1() {
        schTV.setTextColor(Color.WHITE);
        schTV.setBackgroundResource(R.drawable.tab_selected);
        doneTV.setTextColor(Color.BLACK);
        doneTV.setBackground(new ColorDrawable(Color.TRANSPARENT));
    }

    private void tab2() {
        schTV.setTextColor(Color.BLACK);
        schTV.setBackground(new ColorDrawable(Color.TRANSPARENT));
        doneTV.setTextColor(Color.WHITE);
        doneTV.setBackgroundResource(R.drawable.tab_selected);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
