package com.webdealer.otexconnect.goldback.adapters

import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.activities.JobCompleted
import com.webdealer.otexconnect.goldback.models.Order
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.completed_jobs_adapter.*

class CompletedJobsAdapter(private val orders: List<Order>) : RecyclerView.Adapter<CompletedJobsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.completed_jobs_adapter, parent, false))
    }

    override fun getItemCount(): Int {
        return orders.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val order = orders[position]
        holder.title.text = order.title
        holder.loc.text = order.pickup_location
        holder.loc_detail.text = order.dropoff_location
        holder.price.text = order.budget
    }


    inner class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
        init {
            containerView.setOnClickListener {

                val order = orders[adapterPosition]
                val completedJobDetail = Intent(it.context, JobCompleted::class.java)
                completedJobDetail.putExtra("drop", order.dropoff_location)
                completedJobDetail.putExtra("pick", order.pickup_location)
                completedJobDetail.putExtra("num", order.numberOfBoxes)
                completedJobDetail.putExtra("desc", order.description)
                completedJobDetail.putExtra("price", order.budget)
                completedJobDetail.putExtra("driver_id", order.driver_id)
                completedJobDetail.putExtra("fleet_id", order.fleet_id)
                containerView.context.startActivity(completedJobDetail)
            }
        }
    }
}