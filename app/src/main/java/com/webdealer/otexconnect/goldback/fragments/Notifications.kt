package com.webdealer.otexconnect.goldback.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.adapters.NotificationsAdapter
import kotlinx.android.synthetic.main.fragment_notifications.*


class Notifications : BaseFragment() {


    companion object {
        @JvmStatic
        fun newInstance() = Notifications()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        rv.layoutManager = LinearLayoutManager(context)
        rv.adapter = NotificationsAdapter()
    }

}