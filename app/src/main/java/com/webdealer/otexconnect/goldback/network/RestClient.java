package com.webdealer.otexconnect.goldback.network;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.webdealer.otexconnect.goldback.interfaces.OnBalanceListener;
import com.webdealer.otexconnect.goldback.interfaces.OnCompletedJobsListener;
import com.webdealer.otexconnect.goldback.interfaces.OnFCMUpdateListener;
import com.webdealer.otexconnect.goldback.interfaces.OnFavouriteDriversListener;
import com.webdealer.otexconnect.goldback.interfaces.OnFleetOwnerBidsListener;
import com.webdealer.otexconnect.goldback.interfaces.OnFormDataListener;
import com.webdealer.otexconnect.goldback.interfaces.OnJobAddedListener;
import com.webdealer.otexconnect.goldback.interfaces.OnJobAwardedListener;
import com.webdealer.otexconnect.goldback.interfaces.OnLocationListener;
import com.webdealer.otexconnect.goldback.interfaces.OnMessageListener;
import com.webdealer.otexconnect.goldback.interfaces.OnPaymentInfoListener;
import com.webdealer.otexconnect.goldback.interfaces.OnPaymentListener;
import com.webdealer.otexconnect.goldback.interfaces.OnProfileUpdateListener;
import com.webdealer.otexconnect.goldback.interfaces.OnQRCodePostedListener;
import com.webdealer.otexconnect.goldback.interfaces.OnRatingListener;
import com.webdealer.otexconnect.goldback.models.APIResponse;
import com.webdealer.otexconnect.goldback.models.Data;
import com.webdealer.otexconnect.goldback.models.DriverJob;
import com.webdealer.otexconnect.goldback.models.Order;
import com.webdealer.otexconnect.goldback.models.UpdateUser;
import com.webdealer.otexconnect.goldback.models.User;
import com.webdealer.otexconnect.goldback.utils.Utils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Fazal on 3/13/18.
 */

public class RestClient {

    public static Retrofit retrofit = null;
    public static String BASE_URL = "http://goldback.business/api/";
    private static Gson gson;

    private static Retrofit getClient() {
        if (retrofit == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);

            OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
            httpClient.addInterceptor(logging);


            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(httpClient.build())
                    .build();
            gson = new Gson();
        }
        return retrofit;
    }

    public static void stripe(Card card, Context context, TokenCallback tokenCallback) {
        Stripe stripe = new Stripe(context, "pk_test_nJbviIl7bP5H6P2ZziJmPKJU");
        stripe.createToken(card, tokenCallback);
    }

    public static WebService getServices() {

        return getClient().create(WebService.class);
    }


    public static void getCustomerPaymentInfo(String paymentID, OnPaymentInfoListener onPaymentInfoListener) {
        getServices().customerPaymentInfo(paymentID).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Data data = response.body().getData();
                    if (data != null) {
                        Log.i("CustomerPaymentInfoAPI", gson.toJson(response.body()));
                        onPaymentInfoListener.onPaymentInfo(data.payment);
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("CustomerPaymentInfoAPI", t.toString());
            }
        });
    }

    public static void rateDriver(int orderID, String rating, boolean fromFleet, OnRatingListener onRatingListener) {
        Call<APIResponse> call = getServices().rateDriver(orderID, rating);
        String api = "DriverRatingAPI";
        if (fromFleet) {
            call = getServices().rateFleet(orderID, rating);
            api = "FleetRatingAPI";
        }
        String finalApi = api;
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Order order = response.body().order;
                    Log.i(finalApi, gson.toJson(response.body()));
                    onRatingListener.onRated(order);
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e(finalApi, t.toString());
            }
        });
    }

    public static void sendMessage(int orderID, String message, OnMessageListener listener) {
        getServices().sendMessageToDriver(orderID, message).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {

                if (response.isSuccessful()) {
                    Log.i("sendMessageAPI", gson.toJson(response.body()));
                    listener.onMessageSent();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("sendMessageAPI", t.toString());
            }
        });

    }

    public static void deleteFavouriteDriver(int id, OnJobAwardedListener onJobAwardedListener) {

        getServices().deleteFavouriteDriver(id).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("DeleteFavDriversAPI", gson.toJson(response.body()));
                    onJobAwardedListener.onJobAwarded();

                }

            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("DeleteFavDriversAPI", t.toString());
            }
        });
    }

    public static void listFavouriteDrivers(int id, OnFavouriteDriversListener onFavouriteDriversListener) {

        getServices().favouriteDriversList(id).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("ListFavouriteDriversAPI", gson.toJson(response.body()));
                    onFavouriteDriversListener.onFavouriteDriversList(response.body().getData().favourite);

                }

            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("ListFavouriteDriversAPI", t.toString());
            }
        });
    }

    public static void addFavouriteDriver(int id, String driverID, boolean isFleetDriver, OnJobAwardedListener onJobAwardedListener) {
        Call<APIResponse> call = getServices().favouriteDriver(id, driverID);
        String api = "FavouriteDriverAPI";
        if (isFleetDriver) {
            call = getServices().favouriteFleetDriver(id, driverID);
            api = "FavouriteFleetDriverAPI";
        }
        String finalApi = api;
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i(finalApi, gson.toJson(response.body()));
                    onJobAwardedListener.onJobAwarded();

                }

            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e(finalApi, t.toString());
            }
        });
    }

    public static void getDriverLocation(String orderID, OnLocationListener locationListener) {
        getServices().getDriverLocation(orderID).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("DriverLocationAPI", gson.toJson(response.body()));
                    Data data = response.body().getData();
                    if (data.latitude != null && data.longitude != null)
                        locationListener.onDriverLocation(new LatLng(Double.parseDouble(data.latitude), Double.parseDouble(data.longitude)));
                }

            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("DriverLocationAPI", t.toString());
            }
        });
    }

    private static MultipartBody.Part getImageRequestBody(Context context, File file, String key) {
        File compressed = Utils.getCompressedFile(context, file);
        assert compressed != null;
        Log.d("compressedImage", compressed.getPath());
        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), compressed);
        return MultipartBody.Part.createFormData(key, compressed.getName(), mFile);
    }

    private static RequestBody getTextRequestBody(String data) {
        return RequestBody.create(MediaType.parse("text/plain"), data);
    }

    public static void getFormData(OnFormDataListener onFormDataListener) {
        getServices().getFormInfo().enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Data data = response.body().getData();
                    Log.i("getFormInfoAPI", gson.toJson(response.body()));
                    onFormDataListener.onFormData(data.average, data.descriptions);

                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("getFormInfoAPI", t.toString());
            }
        });
    }

    public static void contactUs(int id, String message) {
        getServices().contactUs(id, message).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("ContactUSAPI", gson.toJson(response.body()));


                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("ContactUSApi", t.toString());
            }
        });
    }

    public static void uploadUserImage(Dialog dialog, String uid, File file) {
        getServices().uploadUserImage(getImageRequestBody(dialog.getContext(), file, "profile_image"), getTextRequestBody(uid)).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                Log.i("UserImageAPI", gson.toJson(response.body()));
                if (response.body().getStatus()) {
                    Toast.makeText(dialog.getContext(), "Upload Success", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(dialog.getContext(), "Something went Wrong", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("UserImageAPI", t.toString());
                dialog.dismiss();
            }
        });
    }

    public static void customerPay(String stripeToken, int id, float payment, OnPaymentListener onPaymentListener)

    {
        getServices().customerPay(stripeToken, id, payment).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("PaymentAPI", gson.toJson(response.body()));
                    onPaymentListener.onPay();

                }
            }


            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("PaymentAPI", t.toString());
            }
        });
    }

    public static void deleteOrder(String orderID, OnJobAwardedListener onJobAwardedListener) {
        getServices().deleteOrder(orderID).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("SendOrderToAlLApi", gson.toJson(response.body()));

                    onJobAwardedListener.onJobAwarded();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("SendOrderToAlLApi", t.toString());
            }
        });
    }

    public static void sendJobToAll(String orderID, OnJobAwardedListener onJobAwardedListener) {
        getServices().sendOrderToAll(orderID).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("SendOrderToAlLApi", gson.toJson(response.body()));

                    onJobAwardedListener.onJobAwarded();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("SendOrderToAlLApi", t.toString());
            }
        });
    }

    public static void checkBalance(int id, OnBalanceListener onBalanceListener) {
        getServices().paymentDetail(id).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("PaymentDetailAPI", gson.toJson(response.body()));
                    Data data = response.body().getData();
                    if (data != null) {
                        float balance = 0;
                        if (data.balance.length > 0)
                            balance = data.balance[0];
                        onBalanceListener.onBalance(balance);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("PaymentDetailAPI", t.toString());
            }
        });
    }

    //    private static RequestBody getTextRequestBody(String data) {
//        return RequestBody.create(MediaType.parse("text/plain"), data);
//    }
    public static void addJobToFavourite(int driverID, String fleetID, int id, String title, String desc, String pickUpLoc, String pickUpTime, String dropOffLoc, String dropOffTime, String budget, String boxes, OnJobAddedListener onJobAddedListener) {
        Call<APIResponse> call = getServices().addJobToFavouriteDriver(driverID, id, title, desc, pickUpLoc, pickUpTime, dropOffLoc, dropOffTime, budget, boxes);
        String api = "FavouriteDriverAPI";
        if (fleetID != null) {
            call = getServices().addJobToFavouriteFleet(Integer.valueOf(fleetID), id, title, desc, pickUpLoc, pickUpTime, dropOffLoc, dropOffTime, budget, boxes);
            api = "FavouriteFleetAPI";
        }
        String finalApi = api;
        call.enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i(finalApi, gson.toJson(response.body()));
                    if (response.body().getStatus())
                        onJobAddedListener.onJobAdded(response.body().getData().getJob());

                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e(finalApi, t.toString());
            }
        });
    }

    public static void createQRCodes(List<String> qrCodes, String orderID, OnQRCodePostedListener onQRCodePostedListener) {

//        Log.d("compressedImage", compressed.getPath());
//        RequestBody mFile = RequestBody.create(MediaType.parse("image/png"), compressed);
//        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("imageURL", compressed.getName(), mFile);
        String boxes = "one";
        String qrCodesString = qrCodes.get(0);
        if (qrCodes.size() > 1) {
            boxes = "many";
            qrCodesString = TextUtils.join(",", qrCodes);
        }
        Log.i("QrCodes", qrCodesString);
        Log.i("boxes", boxes);
        getServices().createQRCodes(boxes, qrCodesString, orderID).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Log.i("PostQRAPI", response.body().string());
                        onQRCodePostedListener.onQRCodeSent();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.e("PostQRAPI", t.toString());
            }
        });
    }

    public static void awardJob(DriverJob driverJob, OnJobAwardedListener listener) {
        getServices().awardJob(driverJob).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    try {
                        Log.i("AwardJobAPI", response.body().string());
                        listener.onJobAwarded();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                Log.i("AwardJobAPI", t.toString());
            }
        });
    }

    public static void getFleetOwnerBids(int id, OnFleetOwnerBidsListener listener) {
        getServices().getFleetOwnerBids(id).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("FleetOwnerBidsAPI", gson.toJson(response.body()));
                    listener.onBids(response.body().getData().getDriverDetails());
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("FleetOwnerBidsAPI", t.toString());
            }
        });
    }

    public static void updateFCM(int uid, String token, OnFCMUpdateListener listener) {
        getServices().updateFCM(uid + "", token).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.body().getStatus()) {
                    Log.i("UpdateFCMAPI", "Token Updated");
                    listener.onTokenUpdated();
                }
            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("UpdateFCMAPI", t.toString());
            }
        });
    }

    public static void getDoneJobs(int id, OnCompletedJobsListener listener) {
        getServices().getDoneJobs(id).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.isSuccessful()) {
                    Log.i("CompletedJobsAPI", gson.toJson(response.body()));
                    listener.onJobsCompleted(response.body().getData().getOrders());
                } else listener.onFailed();

            }

            @Override
            public void onFailure(@NonNull Call<APIResponse> call, @NonNull Throwable t) {
                Log.e("CompletedJobsAPI", t.toString());
                listener.onFailed();
            }
        });
    }

    public static void updateProfile(UpdateUser updateUser, OnProfileUpdateListener listener) {
        getServices().updateProfile(updateUser).enqueue(new Callback<User>() {
            @Override
            public void onResponse(@NonNull Call<User> call, @NonNull Response<User> response) {
                if (response.isSuccessful()) {
                    Log.i("ProfileAPI", gson.toJson(response.body()));
                    listener.onProfileUpdated(response.body().getData().getUser());

                }

            }

            @Override
            public void onFailure(@NonNull Call<User> call, @NonNull Throwable t) {
                Log.e("ProfileAPI", t.toString());
            }
        });
    }
}
