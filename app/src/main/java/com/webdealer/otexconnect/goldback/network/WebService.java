package com.webdealer.otexconnect.goldback.network;

import com.webdealer.otexconnect.goldback.models.APIResponse;
import com.webdealer.otexconnect.goldback.models.AddPayment;
import com.webdealer.otexconnect.goldback.models.DriverJob;
import com.webdealer.otexconnect.goldback.models.Login;
import com.webdealer.otexconnect.goldback.models.Signup;
import com.webdealer.otexconnect.goldback.models.Sms;
import com.webdealer.otexconnect.goldback.models.UpdateUser;
import com.webdealer.otexconnect.goldback.models.User;
import com.webdealer.otexconnect.goldback.models.Verify;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by fazal on 3/13/18.
 */

public interface WebService {

    @FormUrlEncoded
    @POST("login-customer")
    Call<Login> login(@Field("email") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("register-customer")
    Call<Signup> signUp(@Field("name") String name, @Field("email") String email, @Field("phone_number") String phoneNumber, @Field("password") String password);


    @FormUrlEncoded
    @POST("send-sms-code")
    Call<Sms> sendSMS(@Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("update-customer-fcm-token")
    Call<APIResponse> updateFCM(@Field("customer_id") String customer_id, @Field("fcm_token") String fcm_token);

    @FormUrlEncoded
    @POST("verify-sms-status")
    Call<Verify> Verify(@Field("id") int id, @Field("type") String type);

    @FormUrlEncoded
    @POST("set-payment-detail")
    Call<AddPayment> addPayment(@Field("customer_id") int id, @Field("cc_name") String cc_name, @Field("cc_number") String cc_number, @Field("cvc_number") String cvc_number, @Field("expiry_month") String expiry_month, @Field("expiry_year") String expiry_year);

    @FormUrlEncoded
    @POST("order-post")
    Call<APIResponse> addJob(@Field("customer_id") int id, @Field("title") String title, @Field("description") String description, @Field("pickup_location") String pickup_location, @Field("pickup_time") String pickup_time, @Field("dropoff_location") String dropoff_location, @Field("delivery_time") String delivery_time, @Field("budget") String budget, @Field("num_of_boxes") String num_of_boxes);

    @FormUrlEncoded
    @POST("order-post-favourite")
    Call<APIResponse> addJobToFavouriteDriver(@Field("driver_id") int driver_id, @Field("customer_id") int id, @Field("title") String title, @Field("description") String description, @Field("pickup_location") String pickup_location, @Field("pickup_time") String pickup_time, @Field("dropoff_location") String dropoff_location, @Field("delivery_time") String delivery_time, @Field("budget") String budget, @Field("num_of_boxes") String num_of_boxes);

    @FormUrlEncoded
    @POST("order-post-favourite")
    Call<APIResponse> addJobToFavouriteFleet(@Field("fleet_id") int fleet_id, @Field("customer_id") int id, @Field("title") String title, @Field("description") String description, @Field("pickup_location") String pickup_location, @Field("pickup_time") String pickup_time, @Field("dropoff_location") String dropoff_location, @Field("delivery_time") String delivery_time, @Field("budget") String budget, @Field("num_of_boxes") String num_of_boxes);

    @FormUrlEncoded
    @POST("pending-orders")
    Call<APIResponse> getScheduledJobs(@Field("user_id") int id);

    @FormUrlEncoded
    @POST("complete-order")
    Call<APIResponse> getDoneJobs(@Field("user_id") int id);

    @FormUrlEncoded
    @POST("order-bid")
    Call<APIResponse> getDriverBids(@Field("order_id") int id);

    @FormUrlEncoded
    @POST("fleet-bids-order")
    Call<APIResponse> getFleetOwnerBids(@Field("order_id") int id);

    @POST("update-customer")
    Call<User> updateProfile(@Body UpdateUser user);

    @POST("assign-job-to-user")
    Call<ResponseBody> awardJob(@Body DriverJob driverJob);


    @POST("createQrCodes")
    Call<ResponseBody> createQRCodes(@Query("boxes") String boxesQuantity, @Query("qrCode") String qrCode, @Query("order_id") String orderID);

    @POST("driverLocation")
    Call<APIResponse> getDriverLocation(@Query("order_id") String orderID);

    @POST("sendMessageToDriver")
    Call<APIResponse> sendMessageToDriver(@Query("order_id") int orderID, @Query("message") String message);

    @POST("give-driver-rating")
    Call<APIResponse> rateDriver(@Query("order_id") int orderID, @Query("rating") String rating);

    @POST("give-fleet-rating")
    Call<APIResponse> rateFleet(@Query("order_id") int orderID, @Query("rating") String rating);

    @POST("balance")
    Call<APIResponse> paymentDetail(@Query("customer_id") int id);

    @POST("customer-payment")
    Call<APIResponse> customerPay(@Query("token") String token, @Query("customer_id") int id, @Query("payment") float payment);

    @POST("customer-payment-info")
    Call<APIResponse> customerPaymentInfo(@Query("payment_id") String paymentID);

    @Multipart
    @POST("customer-profile-image")
    Call<APIResponse> uploadUserImage(@Part MultipartBody.Part imageURL, @Part("customer_id") RequestBody driverID);

    @POST("customerContactus")
    Call<APIResponse> contactUs(@Query("customer_id") int id, @Query("message") String msg);

    @POST("addFavouriteDriver")
    Call<APIResponse> favouriteDriver(@Query("customer_id") int id, @Query("driver_id") String driverID);

    @POST("addFavouriteDriver")
    Call<APIResponse> favouriteFleetDriver(@Query("customer_id") int id, @Query("fleet_driver") String driverID);

    @POST("listOfFavouriteDriver")
    Call<APIResponse> favouriteDriversList(@Query("customer_id") int id);

    @POST("deleteFavouriteDriver")
    Call<APIResponse> deleteFavouriteDriver(@Query("favourite_id") int id);

    @POST("send-order-to-all")
    Call<APIResponse> sendOrderToAll(@Query("order_id") String orderID);

    @POST("delete-order")
    Call<APIResponse> deleteOrder(@Query("order_id") String orderID);

    @GET("getFormInfo")
    Call<APIResponse> getFormInfo();
}
