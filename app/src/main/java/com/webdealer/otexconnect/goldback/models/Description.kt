package com.webdealer.otexconnect.goldback.models

data class Description(val id: Int, val name: String)