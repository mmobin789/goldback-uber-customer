package com.webdealer.otexconnect.goldback.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data {


    public Payment payment;
    @SerializedName("message")
    public String sentMessage;
    public List<FavouriteDriver> favourite;
    public String latitude, longitude;
    public float[] balance;
    public String average;
    public List<Description> descriptions;
    private User_ user;
    @SerializedName("job")
    @Expose
    private Job job;
    @SerializedName("order_id")
    @Expose
    private String order_id;
    @SerializedName("orders")
    @Expose
    private List<Order> orders = null;

    @SerializedName("driver")
    @Expose
    private List<Driver> driverDetails;

    public List<Driver> getDriverDetails() {
        return driverDetails;
    }

    public void setDriverDetails(List<Driver> driverDetails) {
        this.driverDetails = driverDetails;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public User_ getUser() {
        return user;
    }

    public void setUser(User_ user) {
        this.user = user;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }
}