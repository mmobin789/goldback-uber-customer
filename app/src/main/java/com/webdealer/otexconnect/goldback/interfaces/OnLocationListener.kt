package com.webdealer.otexconnect.goldback.interfaces

import com.google.android.gms.maps.model.LatLng

/**
 * Created by MohammedMobinMunir on 5/12/2018.
 */
interface OnLocationListener {
    fun onDriverLocation(latLng: LatLng)
}