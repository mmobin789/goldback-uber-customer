package com.webdealer.otexconnect.goldback.utils

import android.content.Context
import android.os.Environment
import id.zelory.compressor.Compressor
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

object Utils {

    @JvmStatic
    fun meterToMiles(meters: Double) =
            String.format(Locale.getDefault(), "%.2f miles", (meters * 0.000621371))

    fun getCurrentTime(): String {
        val sdf = SimpleDateFormat("hh:mm a", Locale.getDefault())
        return sdf.format(Date())
    }

    @JvmStatic
    fun getCompressedFile(context: Context, file: File): File? {
        try {
            return Compressor(context)
                    //   .setMaxWidth(1500)
                    //   .setMaxHeight(1500)
                    //  .setQuality(60)
                    //  .setCompressFormat(Bitmap.CompressFormat.PNG)
                    .setDestinationDirectoryPath(Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES).absolutePath)
                    .compressToFile(file)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return null
    }
}