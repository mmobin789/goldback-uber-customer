package com.webdealer.otexconnect.goldback.activities

import android.os.Bundle
import android.widget.Toast
import com.google.gson.Gson
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.interfaces.OnJobAwardedListener
import com.webdealer.otexconnect.goldback.models.Order
import com.webdealer.otexconnect.goldback.network.RestClient
import kotlinx.android.synthetic.main.activity_rejected_job.*

class RejectedJob : BaseUI() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rejected_job)
        rejectedJob()
    }

    override fun onBackPressed() {


    }

    private fun rejectedJob() {
        val data = fcmData!!.getString("order")
        val orderID =
                if (data != null) {


                    val orders = Gson().fromJson<Order>(data, Order::class.java)



                    job_row_title.text = orders.title
                    desc.text = orders.description
                    budget.text = orders.budget
                    orders.id.toString()


                } else {
                    desc.text = intent.getStringExtra("desc")

                    intent.getStringExtra("customer_id")
                    job_row_title.text = intent.getStringExtra("title")
                    budget.text = intent.getStringExtra("price")
                    intent.getStringExtra("order_id")
                }

        accept.setOnClickListener { v ->
            progressBar.show()
            RestClient.sendJobToAll(orderID, object : OnJobAwardedListener {
                override fun onJobAwarded() {
                    progressBar.dismiss()
                    Toast.makeText(v.context, "Job Added", Toast.LENGTH_SHORT).show()
                    fcmData = null
                    finish()

                }
            })

        }
        reject.setOnClickListener { v ->
            progressBar.show()
            RestClient.deleteOrder(orderID, object : OnJobAwardedListener {
                override fun onJobAwarded() {
                    progressBar.dismiss()
                    Toast.makeText(v.context, "Job Deleted", Toast.LENGTH_SHORT).show()
                    fcmData = null
                    finish()

                }
            })
        }
    }
}
