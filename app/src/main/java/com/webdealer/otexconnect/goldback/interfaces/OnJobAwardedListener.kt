package com.webdealer.otexconnect.goldback.interfaces

interface OnJobAwardedListener {
    fun onJobAwarded()
}