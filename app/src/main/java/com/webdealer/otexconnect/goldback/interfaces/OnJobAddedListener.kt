package com.webdealer.otexconnect.goldback.interfaces

import com.webdealer.otexconnect.goldback.models.Job

interface OnJobAddedListener {
    fun onJobAdded(job: Job)
}