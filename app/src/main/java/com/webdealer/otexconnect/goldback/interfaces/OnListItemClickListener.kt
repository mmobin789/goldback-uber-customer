package com.webdealer.otexconnect.goldback.interfaces

interface OnListItemClickListener {
    fun onClick(position: Int)
}