package com.webdealer.otexconnect.goldback.fragments

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.adapters.JobTypeAdapter
import kotlinx.android.synthetic.main.fragment_myjobs.*

class MyJobsFragment : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {


        return inflater.inflate(R.layout.fragment_myjobs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val adapter = JobTypeAdapter(childFragmentManager)
        viewpager.adapter = adapter
        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {

            }

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                if (position == 0) {
                    //val fragment = adapter.getItem(position) as ScheduledJobs
                    tab1()
                    //fragment.getScheduledJobs()
                } else {
                    // val fragment = adapter.getItem(position) as CompletedJobs
                    // fragment.getDoneJobs()
                    tab2()

                }
            }
        })



        schTV.setOnClickListener {
            tab1()
            viewpager.currentItem = 0
        }
        doneTV.setOnClickListener {
            tab2()
            viewpager.currentItem = 1
        }

    }

    private fun tab1() {
        schTV.setTextColor(Color.WHITE)
        schTV.setBackgroundResource(R.drawable.tab_selected)
        doneTV.setTextColor(Color.BLACK)
        doneTV.background = ColorDrawable(Color.TRANSPARENT)
    }

    private fun tab2() {
        schTV.setTextColor(Color.BLACK)
        schTV.background = ColorDrawable(Color.TRANSPARENT)
        doneTV.setTextColor(Color.WHITE)
        doneTV.setBackgroundResource(R.drawable.tab_selected)
    }

    companion object {
        @JvmStatic
        fun newInstance(): MyJobsFragment = MyJobsFragment()
    }

}