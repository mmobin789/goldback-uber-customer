package com.webdealer.otexconnect.goldback.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.helper.PrefManager;

public class JobDetailsBids extends BaseUI implements OnMapReadyCallback {


    private TextView tvPickup;
    private TextView tvDropoff;
    private TextView tvBudget;
    private Button btnViewBids;
    private DrawerLayout mDrawerLayout;
    private NavigationView navigation;
    private LatLng pickup, dropOff;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_detail_bidded_on);


        TextView tvDesc = findViewById(R.id.tvJobDes);
        tvPickup = findViewById(R.id.tvPickUpLocation);
        tvDropoff = findViewById(R.id.tvDropOfTime);
        tvBudget = findViewById(R.id.tvPrice);
        btnViewBids = findViewById(R.id.btnViewBids);

        Intent intent = getIntent();
        String mTitle = intent.getStringExtra("title");
        String mDescription = intent.getStringExtra("description");
        String mPickupLocation = intent.getStringExtra("pickup_location");
        String mDropoffLocation = intent.getStringExtra("dropoff_location");
        String mBudget = intent.getStringExtra("budget");

        tvDesc.setText(mDescription);
        tvPickup.setText(mPickupLocation);
        tvDropoff.setText(mDropoffLocation);
        tvBudget.setText(mBudget);
        pickup = encodeAddress(mPickupLocation);
        dropOff = encodeAddress(mDropoffLocation);

        //-----------------------------------------------------


        mDrawerLayout = findViewById(R.id.drawer_layout);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        actionbar.setTitle("");
        // actionbar.setHomeAsUpIndicator(R.drawable.drmenu);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        navigation = findViewById(R.id.nav_view);
        View hView = navigation.getHeaderView(0);
        TextView mNav_title = hView.findViewById(R.id.nav_title);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String mEmail = prefs.getString("mEmail", "email");
        mNav_title.setText(mEmail);

        navigation.setNavigationItemSelectedListener(item -> {

            int id = item.getItemId();
            switch (id) {
                case R.id.nav_logout:

                    Toast.makeText(getApplicationContext(), "Logging out....", Toast.LENGTH_SHORT).show();

                    SharedPreferences sharedPreferences = getSharedPreferences("MyPref", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.clear().apply();
                    PrefManager pref = new PrefManager(getApplicationContext());
                    pref.clearSession();
                    Intent i = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(i);
                    finish();
                    break;
                case R.id.nav_post_job:
                    SharedPreferences prefs1 = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    String payment_id = prefs1.getString("paymentID", "0");
                    //Toast.makeText(getApplicationContext(), paymentID, Toast.LENGTH_SHORT).show();

                    if (payment_id.equals("0")) {


                        Intent intent1 = new Intent(getApplicationContext(), PaymentActivity.class);
                        startActivity(intent1);
                    } else {

                        onBackPressed();
                    }


                default:
                    Toast.makeText(getApplicationContext(), "Under Development", Toast.LENGTH_SHORT).show();
                    break;

            }


            mDrawerLayout.closeDrawer(GravityCompat.START);
            return false;
        });


        //--------------------------------------------------------


        btnViewBids.setOnClickListener(v -> {
            Intent i = new Intent(v.getContext(), BidsActivity.class);
            i.putExtra("id", getIntent().getIntExtra("id", -1));
            startActivity(i);
        });

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

//    private double[] getLatLongFromGivenAddress(String address) {
//        double[] data = new double[2];
//
//        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
//        try {
//            List<Address> addresses = geoCoder.getFromLocationName(address, 1);
//            if (addresses.size() > 0) {
//
//                data[0] = addresses.get(0).getLatitude();
//                data[1] = addresses.get(0).getLongitude();
//
//
//                Log.d("Latitude", "" + data[0]);
//                Log.d("Longitude", "" + data[1]);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        return data;
//    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        //    mMap = googleMap;
        setMapOptions(googleMap);
        drawPolyLines(googleMap, pickup, dropOff);
    }
    //      LatLng barcelona = new LatLng(pickup[0], pickup[1]);
//        mMap.addMarker(new MarkerOptions().position(barcelona).title("Pickup Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.point_a)));

    //    LatLng madrid = new LatLng(dropOff[0], dropOff[1]);
    //       mMap.addMarker(new MarkerOptions().position(madrid).title("Dropoff Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.point_b)));


    //Define list to get all latlng for the route
    //  List<LatLng> path = new ArrayList<>();


    //Execute Directions API request
//        GeoApiContext context = new GeoApiContext.Builder()
//                .apiKey("AIzaSyAl1KSzAAd6ziKl0I7_WdBVEZCPb7_nTkA")
//                .build();
//        String origin = pickup[0] + "," + pickup[1];
//        String destination = dropOff[0] + "," + dropOff[1];
//        DirectionsApiRequest req = DirectionsApi.getDirections(context, origin, destination);
//        try {
//            DirectionsResult res = req.await();
//
//            //Loop through legs and steps to get encoded polylines of each step
//            if (res.routes != null && res.routes.length > 0) {
//                DirectionsRoute route = res.routes[0];
//
//                if (route.legs != null) {
//                    for (int i = 0; i < route.legs.length; i++) {
//                        DirectionsLeg leg = route.legs[i];
//                        if (leg.steps != null) {
//                            for (int j = 0; j < leg.steps.length; j++) {
//                                DirectionsStep step = leg.steps[j];
//                                if (step.steps != null && step.steps.length > 0) {
//                                    for (int k = 0; k < step.steps.length; k++) {
//                                        DirectionsStep step1 = step.steps[k];
//                                        EncodedPolyline points1 = step1.polyline;
//                                        if (points1 != null) {
//                                            //Decode polyline and add points to list of route coordinates
//                                            List<com.google.maps.model.LatLng> coords1 = points1.decodePath();
//                                            for (com.google.maps.model.LatLng coord1 : coords1) {
//                                                path.add(new LatLng(coord1.lat, coord1.lng));
//                                            }
//                                        }
//                                    }
//                                } else {
//                                    EncodedPolyline points = step.polyline;
//                                    if (points != null) {
//                                        //Decode polyline and add points to list of route coordinates
//                                        List<com.google.maps.model.LatLng> coords = points.decodePath();
//                                        for (com.google.maps.model.LatLng coord : coords) {
//                                            path.add(new LatLng(coord.lat, coord.lng));
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        } catch (Exception ex) {
//            Log.e("ERR", ex.getLocalizedMessage());
//        }
//
//        //Draw the polyline
//        if (path.size() > 0) {
//            PolylineOptions opts = new PolylineOptions().addAll(path).color(Color.BLACK).width(6);
//            mMap.addPolyline(opts);
//        }


//--------------------------------------------------------
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        builder.include(barcelona);
//        builder.include(madrid);
//        LatLngBounds bounds = builder.build();
//        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 20));

//----------------------------------------------------

//        LatLngBounds AUSTRALIA = new LatLngBounds(
//                barcelona, madrid);
//
//        mMap.setLatLngBoundsForCameraTarget(AUSTRALIA);
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(barcelona, 13));


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
