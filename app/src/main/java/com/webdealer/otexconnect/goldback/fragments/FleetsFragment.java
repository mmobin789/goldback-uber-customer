package com.webdealer.otexconnect.goldback.fragments;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.adapters.BidsAdapter;
import com.webdealer.otexconnect.goldback.models.APIResponse;
import com.webdealer.otexconnect.goldback.models.Driver;
import com.webdealer.otexconnect.goldback.network.RestClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FleetsFragment extends BaseFragment {

    RecyclerView recycler_view_bids;
    TextView errorTV;

    public FleetsFragment() {
        // Required empty public constructor
    }

    public static FleetsFragment newInstance(int jobID) {
        FleetsFragment fleetsFragment = new FleetsFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("jobID", jobID);
        fleetsFragment.setArguments(bundle);
        return fleetsFragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_fav_drivers, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        recycler_view_bids = view.findViewById(R.id.rv);
        errorTV = view.findViewById(R.id.errorTV);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recycler_view_bids.setLayoutManager(mLayoutManager);
        getDriverBids(getArguments().getInt("jobID"));


    }

    private void getDriverBids(int jobID) {
        RestClient.getServices().getDriverBids(jobID).enqueue(new Callback<APIResponse>() {
            @Override
            public void onResponse(@NonNull Call<APIResponse> call, @NonNull Response<APIResponse> response) {
                if (response.body().getStatus()) {
                    List<Driver> driversList = response.body().getData().getDriverDetails();
                    if (driversList.isEmpty()) {
                        errorTV.setText("No Driver Bidded");
                        errorTV.setVisibility(View.VISIBLE);
                    } else
                        recycler_view_bids.setAdapter(new BidsAdapter(driversList, jobID));


                }
                progressBar.dismiss();
            }

            @Override
            public void onFailure(Call<APIResponse> call, Throwable t) {
                Log.e("error", t.getMessage());
                progressBar.dismiss();

            }
        });
    }


}
