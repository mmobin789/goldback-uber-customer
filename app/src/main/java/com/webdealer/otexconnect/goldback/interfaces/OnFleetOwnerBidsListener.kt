package com.webdealer.otexconnect.goldback.interfaces

import com.webdealer.otexconnect.goldback.models.Driver

interface OnFleetOwnerBidsListener {
    fun onBids(fleetOwnerBids: MutableList<Driver>?)
}