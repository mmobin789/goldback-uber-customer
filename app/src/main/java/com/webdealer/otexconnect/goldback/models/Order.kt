package com.webdealer.otexconnect.goldback.models

import com.google.gson.annotations.SerializedName


data class Order(
        val title: String?, val description: String?, val status: String?, val pickup_location: String?, val dropoff_location: String?, val budget: String?

        , val id: Int,
        val customer_id: String?, val driver_id: String, val fleet_id: String?,


        @SerializedName("num_of_boxes")
        val numberOfBoxes: String?

)