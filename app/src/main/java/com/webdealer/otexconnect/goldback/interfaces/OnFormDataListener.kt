package com.webdealer.otexconnect.goldback.interfaces

import com.webdealer.otexconnect.goldback.models.Description

interface OnFormDataListener {
    fun onFormData(average: String, descriptions: List<Description>)
}