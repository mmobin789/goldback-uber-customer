package com.webdealer.otexconnect.goldback.fragments

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.adapters.FavDriversAdapter
import com.webdealer.otexconnect.goldback.interfaces.OnFavouriteDriversListener
import com.webdealer.otexconnect.goldback.models.FavouriteDriver
import com.webdealer.otexconnect.goldback.network.RestClient
import kotlinx.android.synthetic.main.fragment_fav_drivers.*

class FavouriteDrivers : BaseFragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_fav_drivers, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initFavouriteDriversUI()
    }

    private fun initFavouriteDriversUI() {
        rv.layoutManager = LinearLayoutManager(context)
        progressBar.show()
        RestClient.listFavouriteDrivers(getPrefs().getInt("mId", -1), object : OnFavouriteDriversListener {
            override fun onFavouriteDriversList(favourites: MutableList<FavouriteDriver>?) {
                if (favourites != null && favourites.isNotEmpty()) {
                    rv.adapter = FavDriversAdapter(this@FavouriteDrivers, favourites, false)
                } else errorTV.visibility = View.VISIBLE
                progressBar.dismiss()
            }
        })
    }


    companion object {
        @JvmStatic
        fun newInstance() = FavouriteDrivers()


    }

}