package com.webdealer.otexconnect.goldback.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.webdealer.otexconnect.goldback.R;
import com.webdealer.otexconnect.goldback.models.Login;
import com.webdealer.otexconnect.goldback.models.User_;
import com.webdealer.otexconnect.goldback.network.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseUI implements View.OnClickListener {

    EditText mPasswordEditText;
    EditText mEmailEditText;
    LinearLayout mLoginButton;
    TextView mLoginText;
    Typeface mFont;
    Typeface mFontBold;
    SharedPreferences sp;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();

    }

    public void signUp(View v) {
        startActivity(new Intent(v.getContext(), SignupActivity.class));
    }

    private void initView() {
        mPasswordEditText = findViewById(R.id.password);
        mEmailEditText = findViewById(R.id.email);
        mLoginButton = findViewById(R.id.login);
        mLoginText = findViewById(R.id.logintext);

        mLoginButton.setOnClickListener(this);
        mFont = Typeface.createFromAsset(getAssets(), "fonts/Neris-Light.otf");
        mFontBold = Typeface.createFromAsset(getAssets(), "fonts/Neris-SemiBold.otf");
        mLoginText.setTypeface(mFontBold);
        mPasswordEditText.setTypeface(mFont);
        mEmailEditText.setTypeface(mFont);
        //getSupportActionBar().hide();
    }

    private void storePrefs(String profilePic, int mId, String mName, String mEmail, String mPhone, String payment_id, String ban_status, String email_verification_status, String phone_number_verification_status) {
        SharedPreferences.Editor prefEditor = getPrefs().edit();
        prefEditor.putInt("mId", mId);
        prefEditor.putString("mName", mName);
        prefEditor.putString("mEmail", mEmail);
        prefEditor.putString("mPhone", mPhone);
        prefEditor.putString("paymentID", payment_id);
        prefEditor.putString("ban_status", ban_status);
        prefEditor.putString("email_verification_status", email_verification_status);
        prefEditor.putString("phone_number_verification_status", phone_number_verification_status);
        prefEditor.putString("img", profilePic);

        //prefEditor.putString("fcm_token", fcm_token);
        prefEditor.apply();

    }

//    private boolean login() {
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
//        int mId = prefs.getInt("mId", 0);
//        String mName = prefs.getString("mName", "DefaultName");
//        String mEmail = prefs.getString("mEmail", "email");
//        String mPhone = prefs.getString("mPhone", "phone");
//        String mfck = prefs.getString("mfck", "fck");
//        return mId != 0;
//
//    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.login) {
            if (validate()) {
                progressBar.show();
                login(mEmailEditText.getText().toString(), mPasswordEditText.getText().toString());
            } else {
                Toast.makeText(this, "Please Enter email and password!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean validate() {

        return !mPasswordEditText.getText().toString().equals("") && !mPasswordEditText.getText().toString().isEmpty() && !mEmailEditText.getText().toString().equals("") && !mEmailEditText.getText().toString().isEmpty();
    }

    private void login(String email, String password) {
        String e = email;
        String p = password;

        try {

            RestClient.getServices().login(email, password).enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, Response<Login> response) {
                    progressBar.dismiss();
                    if (response.isSuccessful()) {

                        //-------------------------------
                        Boolean s = response.body().getStatus();
                        if (s) {

                            response.body(); // have your all data
                            User_ user = response.body().getData().getUser();
                            storePrefs(user.profilePic, user.getId(), user.getName(), user.getEmail(), user.getPhoneNumber(), user.getPayment_id(), user.getBanStatus(), user.getEmailVerificationStatus(), user.getPhoneNumberVerificationStatus());


                            if (user.getBanStatus().equals("1")) {
                                Toast.makeText(LoginActivity.this, "Your account is banned by admin", Toast.LENGTH_SHORT).show();


                                return;

                            }


                            if (user.getPhoneNumberVerificationStatus().equals("0")) {
                                Toast.makeText(LoginActivity.this, "Phone Verification Required ", Toast.LENGTH_SHORT).show();

                                Intent intent = new Intent(getApplicationContext(), VerificationActivity.class);
                                startActivity(intent);
                                return;

                            }


                            String mFCM = FirebaseInstanceId.getInstance().getToken();

                            RestClient.updateFCM(user.getId(), mFCM, () -> Toast.makeText(progressBar.getContext(), "FCM Updated", Toast.LENGTH_SHORT).show());


                            Intent intent = new Intent(LoginActivity.this, PostJobActivity.class);
                            startActivity(intent);
                            mEmailEditText.setText("");
                            mPasswordEditText.setText("");
                            closeKeyboard();
                            Toast.makeText(LoginActivity.this, "Login ", Toast.LENGTH_SHORT).show();
                        } else {

//                            APIResponse error = ErrorUtils.parseError(response);
//                            Toast.makeText(getApplicationContext(), error.getStatus().toString(), Toast.LENGTH_SHORT).show();


                            Toast.makeText(getApplicationContext(), "Error: " + response.body().getError().getDescription(), Toast.LENGTH_SHORT).show();


                        }

                        //------------------------------

                    } else {

                        Toast.makeText(getApplicationContext(), "Server Response Error ", Toast.LENGTH_SHORT).show();
                    }


                }

                @Override
                public void onFailure(@NonNull Call<Login> call, @NonNull Throwable t) {
                    progressBar.dismiss();
                    Toast.makeText(LoginActivity.this, "No Internet Connection " + t.getMessage(), Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception ex) {
            Log.e("Login Ex", ex.getMessage());
        }
    }


    public void closeKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(mPasswordEditText.getWindowToken(), 0);
        imm.hideSoftInputFromWindow(mEmailEditText.getWindowToken(), 0);
    }
}
