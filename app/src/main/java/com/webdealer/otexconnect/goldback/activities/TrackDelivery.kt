package com.webdealer.otexconnect.goldback.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.webdealer.otexconnect.goldback.R
import com.webdealer.otexconnect.goldback.interfaces.OnJobAwardedListener
import com.webdealer.otexconnect.goldback.interfaces.OnLocationListener
import com.webdealer.otexconnect.goldback.interfaces.OnRatingListener
import com.webdealer.otexconnect.goldback.models.Order
import com.webdealer.otexconnect.goldback.network.RestClient
import kotlinx.android.synthetic.main.activity_track_delivery.*
import kotlinx.android.synthetic.main.rate_driver.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class TrackDelivery : BaseUI(), OnMapReadyCallback {

    private var orderID = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_delivery)
        orderID = intent.getStringExtra("id")
        budget.text = intent.getStringExtra("budget")
        distance.text = intent.getCharSequenceExtra("distance")
        Log.i("orderID", orderID)
        contact.setOnClickListener {
            val chat = Intent(it.context, ChatActivity::class.java)
            chat.putExtra("order_id", orderID)
            startActivity(chat)
        }
        picked_up.setOnClickListener {
            loadRatingUI(it)
        }
        val mapFragment = map as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    private fun loadRatingUI(v: View) {
        val dialog = Dialog(v.context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.rate_driver)
        val sdf = SimpleDateFormat("MMMM dd,yyyy", Locale.getDefault())
        dialog.time.text = sdf.format(Date())
        val map = dialog.map
        map.onCreate(null)
        map.getMapAsync {
            setMapOptions(it)
            map.onResume()
            val to = intent.getParcelableExtra<LatLng>("to")
            val from = intent.getParcelableExtra<LatLng>("from")
            drawPolyLines(it, from, to)
        }
        dialog.submit_btn.setOnClickListener {
            val rating = dialog.ratingBar.rating
            if (rating > 0) {
                progressBar.show()

                val ratingListener = object : OnRatingListener {
                    override fun onRated(order: Order?) {
                        if (order != null && !order.fleet_id.isNullOrBlank()) {
                            RestClient.rateDriver(orderID.toInt(), rating.toString(), true, this)
                        } else {
                            progressBar.dismiss()
                            dialog.dismiss()
                        }
                    }
                }
                RestClient.rateDriver(orderID.toInt(), rating.toString(), false, ratingListener)
            } else {
                Toast.makeText(it.context, "Rating Required", Toast.LENGTH_SHORT).show()
            }
        }
        dialog.addToFav.setOnClickListener {
            val driverID = intent.getStringExtra("driver_id")
            val id = prefManager.pref.getInt("mId", -1)
            val fleetID = intent.getStringExtra("fleet_id")
            progressBar.show()
            RestClient.addFavouriteDriver(id, driverID, fleetID.isNotBlank(), object : OnJobAwardedListener {
                override fun onJobAwarded() {
                    progressBar.dismiss()
                    Toast.makeText(it.context, "Driver Added To Favourite", Toast.LENGTH_SHORT).show()
                }
            })

        }
        dialog.show()


    }

    private fun getDriverLocation(map: GoogleMap) {

        RestClient.getDriverLocation(orderID, object : OnLocationListener {
            override fun onDriverLocation(latLng: LatLng) {

                map.addMarker(getMapMarker(latLng))
                Handler().postDelayed({
                    getDriverLocation(map)
                    map.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f))

                }, TimeUnit.SECONDS.toMillis(15))

            }
        })

    }

    override fun onMapReady(p0: GoogleMap) {
        setMapOptions(p0)
        getDriverLocation(p0)
    }
}
