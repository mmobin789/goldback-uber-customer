package com.webdealer.otexconnect.goldback.fragments

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.preference.PreferenceManager
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.text.InputType
import android.view.KeyEvent
import android.view.Window
import android.widget.RelativeLayout
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickResult
import com.wang.avi.AVLoadingIndicatorView
import com.webdealer.otexconnect.goldback.R
import kotlinx.android.synthetic.main.payment_dialog.*

abstract class BaseFragment : Fragment() {
    lateinit var progressBar: Dialog
    protected lateinit var paymentID: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        progressBar = createProgressBar(context!!)
        paymentID = getPrefs().getString("paymentID", "0")
    }


    fun getPrefs(): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)

    companion object {
        fun errorDialog(context: Context, header: String, error: String) {
            val dialog = Dialog(context)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.payment_dialog)
            dialog.show()
            dialog.message.text = header
            dialog.etPayment.inputType = InputType.TYPE_NULL
            dialog.etPayment.setText(error)
            dialog.etPayment.isEnabled = false
            dialog.submit.setText(android.R.string.ok)
            dialog.submit.setOnClickListener {
                dialog.dismiss()
            }
        }

        fun createProgressBar(context: Context): Dialog {

            val dialog = Dialog(context)
            dialog.setCancelable(false)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setOnKeyListener { _, _, p2 ->
                if (p2.keyCode == KeyEvent.KEYCODE_BACK) {
                    dialog.dismiss()

                }
                true
            }
            dialog.window.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val relativeLayout = RelativeLayout(context)
            val avLoadingIndicatorView = AVLoadingIndicatorView(context)
            avLoadingIndicatorView.setIndicatorColor(ContextCompat.getColor(context, R.color.colorPrimary))
            //avLoadingIndicatorView.setIndicator(BallPulseIndicator())
            val params = RelativeLayout.LayoutParams(150, 150)
            params.addRule(RelativeLayout.CENTER_IN_PARENT)
            avLoadingIndicatorView.layoutParams = params
            relativeLayout.addView(avLoadingIndicatorView)
            dialog.setContentView(relativeLayout)
            return dialog


        }
    }

    fun pickImage(iPickResult: IPickResult) {
        PickImageDialog.build(PickSetup().setSystemDialog(true), iPickResult).show(childFragmentManager).apply {
            setOnPickCancel { dismiss() }
        }
    }
}

