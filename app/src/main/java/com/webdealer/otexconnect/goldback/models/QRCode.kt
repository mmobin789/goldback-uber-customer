package com.webdealer.otexconnect.goldback.models

import com.google.gson.annotations.SerializedName

data class QRCode(val boxes: String, val qrCode: String, @field:SerializedName("order_id") val orderID: String)