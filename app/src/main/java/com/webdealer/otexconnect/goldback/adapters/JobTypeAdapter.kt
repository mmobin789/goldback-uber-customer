package com.webdealer.otexconnect.goldback.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.webdealer.otexconnect.goldback.fragments.BaseFragment
import com.webdealer.otexconnect.goldback.fragments.CompletedJobs
import com.webdealer.otexconnect.goldback.fragments.ScheduledJobs

class JobTypeAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): BaseFragment {
        return when (position) {
            0 -> ScheduledJobs.newInstance()
            else -> {
                CompletedJobs.newInstance()
            }
        }
    }

    override fun getCount(): Int {
        return 2
    }

}