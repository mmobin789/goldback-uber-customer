package com.webdealer.otexconnect.goldback.interfaces

import com.webdealer.otexconnect.goldback.models.User_

interface OnProfileUpdateListener {
    fun onProfileUpdated(user: User_)
}