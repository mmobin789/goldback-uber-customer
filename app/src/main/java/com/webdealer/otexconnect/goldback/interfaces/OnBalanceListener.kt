package com.webdealer.otexconnect.goldback.interfaces

interface OnBalanceListener {
    fun onBalance(balance: Float)
}